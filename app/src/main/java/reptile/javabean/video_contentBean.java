package reptile.javabean;

public class video_contentBean {
    private String title;//标题
    private String tags;//标签
    private String count;//播放次数
    private String description;//描述
    private String playUrl;//视频地址
    private String datatime;//上架时间
    public video_contentBean(){

    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setPlayUrl(String playUrl) {
        this.playUrl = playUrl;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setCount(String count) {
        this.count = count;
    }

    public void setDatatime(String datatime) {
        this.datatime = datatime;
    }

    public void setTags(String tags) {
        this.tags = tags;
    }

    public String getPlayUrl() {
        return playUrl;
    }

    public String getDescription() {
        return description;
    }

    public String getTitle() {
        return title;
    }

    public String getCount() {
        return count;
    }

    public String getDatatime() {
        return datatime;
    }

    public String getTags() {
        return tags;
    }
}

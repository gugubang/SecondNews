package reptile.javabean;

public class pengpai_videocontentbean {
    private String title;//标题
    private String source;//作者来源
    private String content;//描述
    private String playUrl;//视频地址
    private String time;
    public pengpai_videocontentbean(){

    }

    public String getSource() {
        return source;
    }

    public String getContent() {
        return content;
    }

    public String getTitle() {
        return title;
    }

    public String getPlayUrl() {
        return playUrl;
    }

    public String getTime() {
        return time;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public void setPlayUrl(String playUrl) {
        this.playUrl = playUrl;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setTime(String time) {
        this.time = time;
    }
}

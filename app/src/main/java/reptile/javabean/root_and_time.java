package reptile.javabean;

public class root_and_time {
    //新闻详情页的新闻来源和链接网址实体类
    private String href;
    private String rootandtime;

    public root_and_time() {
        super();
    }

    public root_and_time(String href, String rootandtime) {
        super();
        this.href = href;
        this.rootandtime= rootandtime;
    }

    public String getHref() {
        return href;
    }

    public void setHref(String href) {
        this.href = href;
    }

    public String getRootandtime() {
        return rootandtime;
    }

    public void setRootandtime(String rootandtime) {
        this.rootandtime = rootandtime;
    }

    @Override
    public String toString() {
        return "root_and_time [href=" + href + ", rootandtime=" + rootandtime + "]";
    }
}

package reptile.javabean;

import android.content.Intent;

public class RoolImageBean {
    private Integer ImageURL;
    private String ImageToHref;
    public RoolImageBean(){

    }
    public RoolImageBean(Integer ImageURL, String ImageToHref){
        this.ImageURL=ImageURL;
        this.ImageToHref=ImageToHref;
    }

    public void setImageToHref(String imageToHref) {
        ImageToHref = imageToHref;
    }

    public void setImageURL(Integer imageURL) {
        ImageURL = imageURL;
    }

    public String getImageToHref() {
        return ImageToHref;
    }

    public Integer getImageURL() {
        return ImageURL;
    }
}

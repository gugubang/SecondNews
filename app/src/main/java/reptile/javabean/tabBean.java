package reptile.javabean;
//新闻分类名称以及相关链接的实体类
public class tabBean {
    private  String tabName;
    private  String tabHref;
    public tabBean(){
        super();
    }
    public tabBean(String tabName,String tabHref){
        super();
        this.tabName=tabName;
        this.tabHref=tabHref;
    }

    public void setTabName(String tabName) {
        this.tabName = tabName;
    }

    public String getTabName() {
        return tabName;
    }

    public void setTabHref(String tabHref) {
        this.tabHref = tabHref;
    }

    public String getTabHref() {
        return tabHref;
    }

    @Override
    public String toString() {
        return  "tabBean [tabName=" + tabName+ ", tabHref=" + tabHref + "]";
    }
}

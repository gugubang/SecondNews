package reptile.javabean;

import java.util.List;
//爬取文章详细数据的实体类
public class contentBean {
    private String title; // 文章标题
    private String rtime;//时间和来源
    private StringBuffer context; // 文章内容
    private String image;  //文章头图片
    private String oldtitle;//原标题
    private String author; // 文章作者信息
    public contentBean(){
        super();
    }
    public contentBean(String title,String rtime,StringBuffer context,String image,String author,String oldtitle){
        super();
        this.title=title;
        this.rtime=rtime;
        this.context=context;
        this.image=image;
        this.author=author;
        this.oldtitle=oldtitle;
    }

    public void setOldtitle(String oldtitle) {
        this.oldtitle = oldtitle;
    }

    public String getOldtitle() {
        return oldtitle;
    }

    public void setTitle(String title){
        this.title=title;
    }

    public void setRtime(String rtime) {
        this.rtime = rtime;
    }

    public void setContext(StringBuffer context) {
        this.context = context;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getTitle() {
        return title;
    }

    public String getRtime() {
        return rtime;
    }

    public StringBuffer getContext() {
        return context;
    }

    public String getImage() {
        return image;
    }

    public String getAuthor() {
        return author;
    }

    @Override
    public String toString() {
        return "contentBean [title=" + title +  ", rtime=" + rtime + ", context=" + context + ", image="
                + image + ", author=" + author + "]";
    }
}

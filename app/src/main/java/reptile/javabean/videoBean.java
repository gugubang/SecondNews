package reptile.javabean;

public class videoBean {
    private String title;//标题
    private String description;//描述
    private String playUrl;//播放内容地址
    private String datatime;//发布时间
    private String count;//评分
    private String backgroundurl;//背景图地址
    private String time;//时长
    private String videourl;
    public videoBean(){

    }
    public videoBean(String title, String description, String playUrl, String backgroundurl, String time){
        this.backgroundurl=backgroundurl;
        this.time=time;
        this.description=description;
        this.playUrl=playUrl;
        this.title=title;
    }

    public void setVideourl(String videourl) {
        this.videourl = videourl;
    }

    public String getVideourl() {
        return videourl;
    }

    public void setBackgroundurl(String backgroundurl) {
        this.backgroundurl = backgroundurl;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public void setDatatime(String datatime) {
        this.datatime = datatime;
    }

    public void setCount(String count) {
        this.count = count;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setPlayUrl(String playUrl) {
        this.playUrl = playUrl;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getTitle() {
        return title;
    }

    public String getBackgroundurl() {
        return backgroundurl;
    }

    public String getTime() {
        return time;
    }

    public String getDatatime() {
        return datatime;
    }

    public String getCount() {
        return count;
    }

    public String getDescription() {
        return description;
    }

    public String getPlayUrl() {
        return playUrl;
    }

}

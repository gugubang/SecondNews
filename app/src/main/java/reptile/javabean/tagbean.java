package reptile.javabean;
//新闻详情的关键字和相关链接的实体类
public class tagbean {
    private String href;
    private String tagname;

    public tagbean() {
        super();
    }

    public tagbean(String href, String tagname) {
        super();
        this.href = href;
        this.tagname = tagname;
    }

    public String getHref() {
        return href;
    }

    public void setHref(String href) {
        this.href = href;
    }

    public String getTagname() {
        return tagname;
    }

    public void setTagname(String tagname) {
        this.tagname = tagname;
    }

    @Override
    public String toString() {
        return "TagBean [href=" + href + ", tagname=" + tagname + "]";
    }

}

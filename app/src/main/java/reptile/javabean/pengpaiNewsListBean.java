package reptile.javabean;

//爬取一个分类下的新闻列表数据实体类
public class pengpaiNewsListBean {
    private String imgurl; // 文章列表缩略图片
    private String href; // 文章地址
    private String title; // 文章标题
    private StringBuffer tag;//文章关键字集合
    private String datetime; // 文章发表时间
    private String Source;  //文章来源
    private String Content;
    private String commentURL;
    public pengpaiNewsListBean(){
        super();
    }
    public pengpaiNewsListBean(String imgurl, String href, String title, String Source, StringBuffer tag, String datetime ){
        super();
        this.imgurl = imgurl;
        this.href = href;
        this.title = title;
        this.datetime = datetime;
        this.tag=tag;
        this.Source=Source;
    }

    public void setContent(String content) {
        Content = content;
    }

    public String getContent() {
        return Content;
    }

    public String getCommentURL() {
        return commentURL;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDatetime() {
        return datetime;
    }

    public void setSource(String source) {
        Source = source;
    }

    public String getSource() {
        return Source;
    }

    public void setDatetime(String datetime) {
        this.datetime = datetime;
    }

    public String getImgurl() {
        return imgurl;
    }

    public void setImgurl(String imgurl) {
        this.imgurl = imgurl;
    }

    public StringBuffer getTag() {
        return tag;
    }

    public void setTag(StringBuffer tag) {
        this.tag = tag;
    }

    public void setCommentURL(String commentURL) {
        this.commentURL = commentURL;
    }

    public String getHref(){ return href; }

    public void setHref(String href){ this.href=href; }

    @Override
    public String toString() {
        return  "NewsList [imgurl=" + imgurl +  ", href=" + href + ", title=" + title + ", datetime="
                + datetime + ", tag=" + tag + "]";
    }
}

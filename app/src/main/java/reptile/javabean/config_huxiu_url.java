package reptile.javabean;
import java.util.ArrayList;
import java.util.List;

public class config_huxiu_url {
    public config_huxiu_url(){

    }
    public static List<tabBean> getlist(){
        List<tabBean> list=new ArrayList<>();
        list.add(new tabBean("全球热点","https://www.huxiu.com/channel/107.html"));
        list.add(new tabBean("生活腔调","https://www.huxiu.com/channel/4.html"));
        list.add(new tabBean("年轻一代","https://www.huxiu.com/channel/106.html"));
        list.add(new tabBean("前沿科技","https://www.huxiu.com/channel/105.html"));
        list.add(new tabBean("娱乐淘金","https://www.huxiu.com/channel/22.html"));
        list.add(new tabBean("十亿消费者","https://www.huxiu.com/channel/103.html"));
        list.add(new tabBean("文化教育","https://www.huxiu.com/channel/113.html"));
        list.add(new tabBean("医疗健康","https://www.huxiu.com/channel/111.html"));
        list.add(new tabBean("社交通讯","https://www.huxiu.com/channel/112.html"));
        list.add(new tabBean("金融地产","https://www.huxiu.com/channel/102.html"));
        return list;
    }
}

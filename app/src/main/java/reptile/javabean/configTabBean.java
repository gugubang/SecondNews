package reptile.javabean;

import java.util.ArrayList;
import java.util.List;
//每个分类的标签对象，包括网址
public class configTabBean {
    public static List<tabBean> getTabBean(){
        List<tabBean> tabs=new ArrayList<tabBean>();
        tabs.add(new tabBean("要闻","https://temp.163.com/special/00804KVA/cm_yaowen20200213.js"));
        tabs.add(new tabBean("场库","https://www.vmovier.com/"));
        tabs.add(new tabBean("国内","https://temp.163.com/special/00804KVA/cm_guonei.js"));
        tabs.add(new tabBean("国际","https://temp.163.com/special/00804KVA/cm_guoji.js"));
        tabs.add(new tabBean("独家","https://temp.163.com/special/00804KVA/cm_dujia.js"));
        tabs.add(new tabBean("军事","https://temp.163.com/special/00804KVA/cm_war.js"));
        tabs.add(new tabBean("财经","https://temp.163.com/special/00804KVA/cm_money.js"));
        tabs.add(new tabBean("科技","https://temp.163.com/special/00804KVA/cm_tech.js"));
        tabs.add(new tabBean("体育","https://temp.163.com/special/00804KVA/cm_sports.js"));
        tabs.add(new tabBean("娱乐","https://temp.163.com/special/00804KVA/cm_ent.js"));
        tabs.add(new tabBean("时尚","https://temp.163.com/special/00804KVA/cm_lady.js"));
        tabs.add(new tabBean("汽车","https://temp.163.com/special/00804KVA/cm_auto.js"));
        tabs.add(new tabBean("房产","https://temp.163.com/special/00804KVA/cm_houseguangzhou.js"));
        tabs.add(new tabBean("航空","https://temp.163.com/special/00804KVA/cm_hangkong.js"));
        tabs.add(new tabBean("健康","https://temp.163.com/special/00804KVA/cm_jiankang.js"));
        return tabs;
    }
}

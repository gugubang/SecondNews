package reptile.Manager;

import android.util.Log;

import com.example.zzb.secondnews.R;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import reptile.javabean.*;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
//新闻详情的爬取的逻辑方法
public class contentBeanManager {
    public contentBeanManager(){

    }

    public contentBean getContent(String URL)throws IOException {

        Document doc= Jsoup.connect(URL).get();
        contentBean contentBean=new contentBean();
        root_and_time rtime=new root_and_time("","");
       // List<contentBean> contentBeanList=new ArrayList<contentBean>();
        if(doc.select("div.post_content_main>h1").isEmpty())
        { contentBean.setTitle(""); }
       else{ String title=doc.select("div.post_content_main>h1").first().text();
        contentBean.setTitle(title);}

        if(doc.select("div.post_time_source").isEmpty())
        { contentBean.setRtime(""); }
        else {
        String rootandtime=doc.select("div.post_time_source").first().text().replace("举报","");
        contentBean.setRtime(rootandtime);}
        //System.out.println(rootandtime);
        //System.out.println(rtime);
        StringBuffer content=new StringBuffer();
        StringBuffer secondcontent=new StringBuffer();
       List<String>  emptiycontent=new ArrayList<>();
      StringBuffer contentfail=new StringBuffer("加载失败</br>");
       if(doc.select("div.post_text>p").isEmpty())
       { contentBean.setContext(contentfail); }
       else{ Elements elements=doc.select("div.post_text>p");
        for(Element ele:elements){
               content.append("     "+ele.text()+"</br>");
            //System.out.println(ele.text());
            }
           String mycontent=content.toString();
           String mycontent2=mycontent.substring(0,mycontent.lastIndexOf("</br>"));
           contentBean.setContext(new StringBuffer(mycontent2));
       }
        if(contentBean.getContext()==contentfail){
           if(doc.select("div[id=endText]>p").isEmpty())
               contentBean.setContext(contentfail);
           else{ Elements elements=doc.select("div[id=endText]>p");
            for(Element ele:elements){
                secondcontent.append("     "+ele.text()+"</br>");
               System.out.print(ele.text());
            }
            String mycontent=secondcontent.toString();
            String mycontent2=mycontent.substring(0,mycontent.lastIndexOf("</br>"));
            contentBean.setContext(new StringBuffer(mycontent2));
           }
        }

        if(doc.select("p.f_center>img").isEmpty()){
           if(doc.select("p[align=center]>img").isEmpty())
           {
               contentBean.setImage("https://ss0.bdstatic.com/70cFuHSh_Q1YnxGkpoWK1HF6hhy/it/u=386535230,3956809074&fm=26&gp=0.jpg");
           }

        }else {
            String img=doc.select("p.f_center>img").first().attr("src");
            contentBean.setImage(img);
        }


        if(doc.select("span.ep-editor").isEmpty())
        {
            contentBean.setAuthor("匿名");
        }else {
        String author=doc.select("span.ep-editor").first().text();
        contentBean.setAuthor(author);}

      return contentBean;
    }
}

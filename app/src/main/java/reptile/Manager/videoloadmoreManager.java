package reptile.Manager;

import java.util.ArrayList;
import java.util.List;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import reptile.javabean.videoBean;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

public class videoloadmoreManager {
    public videoloadmoreManager(){

    }

    public List<videoBean> getmore(String body){
        List<videoBean> list=new ArrayList<videoBean>();
        JSONObject jsonObject= JSON.parseObject(body);
        String infor=jsonObject.getString("data");
        Document doc= Jsoup.parse(infor);
        Elements elements=doc.select("li[class=clearfix]");
        for(Element ele:elements) {
            videoBean Bean=new videoBean();
            String title=ele.select("h1").text();
            Bean.setTitle(title);
            String des=ele.select("div[class=index-intro]").text();
            Bean.setDescription(des);
            String href=ele.select("div[class=index-intro]>a").attr("href");
            Bean.setPlayUrl("https://www.vmovier.com/"+href);
            String imgurl=ele.select("a>img").attr("src");
            Bean.setBackgroundurl(imgurl);
            String time=ele.select("span[class=film-time]").text();
            Bean.setTime(time);
            String count=ele.select("div[class=rating]").attr("data-score");
            Bean.setCount(count);
            list.add(Bean);
        }
        return list;
    }
}

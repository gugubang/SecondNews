package reptile.Manager;
import reptile.javabean.video_contentBean;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.util.regex.Pattern;

public class video_contentManager {
    public video_contentManager(){

    }
    public video_contentBean getcontent(String URL) throws IOException {
        video_contentBean Bean=new video_contentBean();
        Document doc= Jsoup.connect(URL).get();
        String title=doc.select("div[class=title-wrap]>h3").text();
        Bean.setTitle(title);
        String tags=doc.select("span[class=cate v-center]").text();
        Bean.setTags(tags);
        String count=doc.select("i[class=fs_12 fw_300 c_b_6 v-center play-counts]").text();
        Bean.setCount(count);
        String des=doc.select("p[class=desc line-hide fs_14 c_b_3 fw_300 line-hide-3]").text();
        des=des.split("From")[0];
        Bean.setDescription(des);
        String videourl=doc.select("source").toString();
        Elements elements=doc.select("script");
        String time=doc.select("span[class=update-time v-center]>i").text();
        Bean.setDatatime(time);
        String data= doc.body().toString().split("vid:")[1].split(",")[0];
        data=data.substring(2,data.length()-1);
        Bean.setPlayUrl("https://openapi-vtom.vmovier.com/video/"+data);
        //System.out.println(data);
        return  Bean;
    }
}

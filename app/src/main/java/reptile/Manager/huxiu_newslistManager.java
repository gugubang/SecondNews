package reptile.Manager;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import reptile.javabean.NewsListBean;

public class huxiu_newslistManager {
    public huxiu_newslistManager(){

    }
    public List<NewsListBean> getlist(String url) throws IOException {
        Document doc= Jsoup.connect(url).get();
        String str=doc.select("script").first().toString();
        String str1=str.split("__=")[1];
        String json=str1.substring(0,str1.lastIndexOf(";(function()"));
        List<NewsListBean> beans=new ArrayList<NewsListBean>();
        if(json==null)
            return null ;
        JSONObject jsonObject= JSON.parseObject(json);
        if(jsonObject==null)
            return null;
        JSONObject channel=jsonObject.getJSONObject("channel");
        JSONObject channels=channel.getJSONObject("channels");
        JSONArray jsonArray=channels.getJSONArray("datalist");
        //System.out.println(jsonArray);
        for(int i=0;i<jsonArray.size();i++){
            NewsListBean b=new NewsListBean();
            JSONObject object=jsonArray.getJSONObject(i);
            JSONObject user=object.getJSONObject("user_info");
            String title=object.getString("title");
            String img=object.getString("origin_pic_path");
            String time=object.getString("formatDate");
            String href=object.getString("share_url");
            String soure=user.getString("username");
            b.setImgurl(img);
            b.setHref(href);
            b.setTitle(title);
            b.setSource(soure);
            b.setDatetime(time);
            beans.add(b);
            //System.out.println(title+"\n"+img+"\n"+time+"\n"+href+"\n"+soure);
        }

        return beans;
    }
}

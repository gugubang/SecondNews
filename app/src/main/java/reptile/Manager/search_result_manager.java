package reptile.Manager;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import java.util.ArrayList;
import java.util.List;

import reptile.javabean.NewsListBean;
import reptile.tools.SslUtil;

public class search_result_manager {
    public search_result_manager(){

    }
    public List<NewsListBean> getresult(String URL) throws Exception {
        List<NewsListBean> bean=new ArrayList<NewsListBean>();
        SslUtil.ignoreSsl();
        Document doc= Jsoup.connect(URL).get();
        Elements elements=doc.select("li[class=image]");
        for(Element element:elements)
        {
            NewsListBean news=new NewsListBean();
            String title=element.select("h3[class=tit]").text();
            String img=element.select("p[class=bre]>img").attr("src");
            String href=element.select("h3[class=tit]>span").attr("lanmu1");
            String Content=element.select("p[class=bre]").text();
            String Source=element.select("span[class=src]").text();
            String time=element.select("span[class=tim]").text();
            news.setTitle(title);
            news.setContent(Content);
            news.setHref(href);
            news.setImgurl(img);
            news.setSource(Source);
            news.setDatetime(time);
            bean.add(news);
            //System.out.println(Content+"\n"+Source+"\n"+time);
        }
        return bean;
    }
}

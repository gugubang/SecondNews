package reptile.Manager;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import reptile.javabean.videoBean;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class VideoListManager {
    public VideoListManager(){

    }

    public List<videoBean> getVideoList(String URL) throws IOException {
        Document doc= Jsoup.connect(URL).get();
        List<videoBean> videoList=new ArrayList<videoBean>();
        Elements elements=doc.select("li[class=clearfix]");
        for(Element ele:elements)
        {
            videoBean Bean=new videoBean();
            String title=ele.select("h1").text();
            Bean.setTitle(title);
            String des=ele.select("div[class=index-intro]").text();
            Bean.setDescription(des);
            String href=ele.select("div[class=index-intro]>a").attr("abs:href");
            Bean.setPlayUrl(href);
            String imgurl=ele.select("a>img").attr("src");
            Bean.setBackgroundurl(imgurl);
            String time=ele.select("span[class=film-time]").text();
            Bean.setTime(time);
            String datatime=ele.select("span[class=time]").text();
            Bean.setDatatime(datatime);
            String count=ele.select("div[class=rating]").attr("data-score");
            Bean.setCount(count);
            String videourl=new video_contentManager().getcontent(href).getPlayUrl();
            Bean.setVideourl(videourl);
            videoList.add(Bean);
        }
        return videoList;

    }
}

package reptile.Manager;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import reptile.javabean.pengpaiNewsListBean;
public class pengpaiNweslistManager {
    public pengpaiNweslistManager(){

    }
    public List<pengpaiNewsListBean> getlist(String URL) throws IOException {
        Document doc= Jsoup.connect(URL).get();
        List<pengpaiNewsListBean> news=new ArrayList<pengpaiNewsListBean>();
        if(URL.contains("channel_26916")){
            Elements elements=doc.select("li[class=video_news]");
            for(Element element:elements)
            {
                pengpaiNewsListBean bean = new pengpaiNewsListBean();
                String title=element.select("div[class=video_title]").text();
                String href=element.select("div[class=video_list_pic]>a").first().attr("href");
                String content=element.select("p").first().text();
                String img=element.select("div[class=video_list_pic]>img").attr("src");
                String source=element.select("div[class=t_source]>a").text();
                String time=element.select("div[class=t_source]>span").first().text();
                bean.setTitle(title);
                bean.setContent(content);
                bean.setImgurl(img);
                bean.setHref("https://www.thepaper.cn/"+href);
                bean.setSource(source);
                bean.setDatetime(time);
                news.add(bean);
                System.out.println(time);
            }

        }else {
            Elements elements = doc.select("div[class=news_li]");
            for (int j = 0; j < (elements.size() - 1) / 2; j++) {
                Element element = elements.get(j);
                pengpaiNewsListBean bean = new pengpaiNewsListBean();
                String title = element.select("h2>a").text();
                String href = element.select("h2>a").attr("href");
                String content = element.select("p").text();
                String img = element.select("a[class=tiptitleImg]>img").attr("src");
                String source = element.select("div[class=pdtt_trbs]>a").text();
                String time = element.select("div[class=pdtt_trbs]>span").first().text();
                bean.setTitle(title);
                bean.setContent(content);
                bean.setImgurl(img);
                bean.setHref("https://www.thepaper.cn/" + href);
                bean.setSource(source);
                bean.setDatetime(time);
                if(!source.equals("上直播"))
                    news.add(bean);
                //System.out.println("https://www.thepaper.cn/"+href);
                //System.out.println("z这是"+i);

            }
        }
        return news;
    }
}

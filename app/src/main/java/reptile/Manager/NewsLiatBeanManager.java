package reptile.Manager;

import android.content.Intent;
import android.util.Log;
import android.view.View;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.bumptech.glide.Glide;
import com.example.zzb.secondnews.NewsContent;
import reptile.javabean.contentBean;
import reptile.Manager.contentBeanManager;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import reptile.javabean.*;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
//新闻列表的数据爬取的逻辑方法
public class NewsLiatBeanManager {
    private StringBuffer empity=new StringBuffer(" ");
public NewsLiatBeanManager(){

}

public List<NewsListBean> getNewsList(String body){
    JSONArray jsonArray = JSON.parseArray(body);
    final List<NewsListBean> NewsList=new ArrayList<NewsListBean>();
    for(int i=0;i<jsonArray.size();i++){
        List<tagbean> tagbeans=new ArrayList<tagbean>();
        final NewsListBean news=new NewsListBean();
        JSONObject data = jsonArray.getJSONObject(i);
        String imgurl=data.getString("imgurl");
        //System.out.println(imgurl);
        news.setImgurl(imgurl);
        String title=data.getString("title");
        news.setTitle(title);
        String href=data.getString("docurl");
        news.setHref(href);
        String commenturl=data.getString("commenturl");
        news.setCommentURL(commenturl);
        String datatime=data.getString("time");
        news.setDatetime(datatime);
        String Source=data.getString("source");
        news.setSource(Source);
        JSONArray tagJson=data.getJSONArray("keywords");
        StringBuffer tags=new StringBuffer();
       for(int j=0;j<tagJson.size();j++) {
            tagbean mytag=new tagbean();
            JSONObject tag=tagJson.getJSONObject(j);
            tagbeans.add(new tagbean(tag.getString("keyname"),tag.getString("akey_link")));
           if(tag.getString("keyname")==null)
           {
               tags.append(empity);
               break;
           }
            tags.append(tag.getString("keyname"));
           tags.append(empity);
        }
            news.setTag(tags);
        //System.out.println(tagbeans);
        if(href.contains("lady.163")){
            if(href.contains("v.163"))//屏蔽视频新闻
                continue;
            if(href.contains("c.m.163"))
                continue;
            if(href.contains("photoview"))
                continue;
            NewsList.add(news);
        }else {
        if(href.contains("v.163"))//屏蔽视频新闻
            continue;
        if(href.contains("dy.163"))
            continue;
        if(href.contains("c.m.163"))
            continue;
        if(href.contains("photoview"))
            continue;
        NewsList.add(news);}



    }
    //System.out.println(NewsList);
    return NewsList;
 }
}

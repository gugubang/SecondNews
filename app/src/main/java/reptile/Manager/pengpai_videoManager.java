package reptile.Manager;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import java.io.IOException;

import reptile.javabean.pengpai_videocontentbean;

public class pengpai_videoManager {
    public pengpai_videoManager(){

    }
    public pengpai_videocontentbean getvideo(String URL) throws IOException {
        pengpai_videocontentbean bean=new pengpai_videocontentbean();
        Document doc= Jsoup.connect(URL).get();
        String plsyurl=doc.select("div[class=video_news_detail video_w1200]>video>source").attr("src");
        String title=doc.select("div[class=video_txt_t]>h2").text();
        String content=doc.select("div[class=video_txt_l]>p").first().text();
        String time=doc.select("div[class=video_info_left]").text();
        String Source=doc.select("div[class=video_info_second]").text();
        bean.setPlayUrl(plsyurl);
        bean.setTitle(title);
        bean.setContent(content);
        bean.setTime(time);
        bean.setSource(Source);
        return bean;
    }
}

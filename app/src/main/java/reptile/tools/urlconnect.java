package reptile.tools;


import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.jsoup.nodes.Document;
import java.io.IOException;

public class urlconnect {
    /*
    输入某个分类网易新闻的异步加载的js链接，返回得到的response字符串，不是标准的Jason字符串，需要剪辑
    */

    public urlconnect(){

    }
    public  static final String URlcennect(String URL)throws IOException {
        CloseableHttpClient httpClient= HttpClients.createDefault();
        String doc="";
        HttpGet httpGet=new HttpGet(URL);
        try{
            HttpResponse response=httpClient.execute(httpGet);
            if (response.getStatusLine().getStatusCode() == 200){
                HttpEntity entity= response.getEntity();
                 doc=EntityUtils.toString(entity,"GBK");

            }
        }catch (IOException e){
            e.printStackTrace();
        }
        return doc;
    }
}

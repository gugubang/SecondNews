package reptile.tools;

public class StringParse {
    /*
    针对网易新闻的json字符串进行掐头去尾，转换成标准json字符串
     */
    public static final String parse(String body){
        if(body!="") {
            body = body.replace("data_callback(", "");
            body = body.substring(0, body.lastIndexOf(")"));
            return body;
        }
        return null;
    }
    public StringParse(){
        super();
    }
}

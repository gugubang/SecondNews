package Fragment;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.zzb.secondnews.NewsContent;
import com.example.zzb.secondnews.everyDay;
import com.example.zzb.secondnews.R;
import com.scwang.smartrefresh.layout.SmartRefreshLayout;
import com.scwang.smartrefresh.layout.api.RefreshLayout;
import com.scwang.smartrefresh.layout.listener.OnLoadmoreListener;
import com.scwang.smartrefresh.layout.listener.OnRefreshListener;
import com.youth.banner.Banner;
import com.youth.banner.listener.OnBannerListener;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import reptile.Manager.NewsLiatBeanManager;
import reptile.javabean.NewsListBean;
import reptile.javabean.tabBean;
import reptile.javabean.configRoolImage;
import reptile.javabean.RoolImageBean;
import  reptile.tools.*;
import Adapter.FirstFragmentListAdapter;
import java.io.IOException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

public class FirstFragment extends Fragment {//第一个页面单独添加布局，实现热点图片轮播，列表下拉刷新，上方拉加载更多。
    private View view;
    private ListView listView;
    private SmartRefreshLayout smartRefreshLayout;
    private Banner banner;
    private tabBean tabBean;
    private Boolean isload=true;
    private Boolean isrefresh=true;
    private List<NewsListBean> NewsliatBeans;
    private FrameLayout my_loading_frameyout;
    private LinearLayout loading,empty,error,always;
    private View headerView;
     private FirstFragmentListAdapter adapter;
    public static  List<String>  titlelist=new ArrayList<>();;
    public void setTabBean(reptile.javabean.tabBean tabBean) {
        this.tabBean = tabBean;
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view= inflater.inflate(R.layout.first_fragment,container,false);
        headerView=inflater.inflate(R.layout.list_view_header,null);
        listView=view.findViewById(R.id.list);
        always=(LinearLayout) view.findViewById(R.id.my_firstpage);
        loading=(LinearLayout)view.findViewById(R.id.loading);
        empty=(LinearLayout)view.findViewById(R.id.empty);
        error=(LinearLayout)view.findViewById(R.id.error);
        smartRefreshLayout=(SmartRefreshLayout)view.findViewById(R.id.smart);
        banner=(Banner)headerView.findViewById(R.id.banner);//轮播控件实例化
        my_loading_frameyout=(FrameLayout) view.findViewById(R.id.first_Fragment_Loading);
        listView.addHeaderView(headerView);//把banner控件添加到Listview,跟随其一起滑动
        init();
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                NewsListBean news=NewsliatBeans.get(position-1);
                Log.d("zzzzzzzzzz",news.getSource());
                Intent intent=new Intent(getActivity(),NewsContent.class);
                intent.putExtra("url",news.getHref());
                intent.putExtra("title",news.getTitle());
                intent.putExtra("image",news.getImgurl());
                intent.putExtra("commenturl",news.getCommentURL());
                startActivity(intent);
                Toast.makeText(getActivity(),"点击了"+news.getSource(),Toast.LENGTH_SHORT).show();
            }
        });
        listView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                AlertDialog.Builder builder=new AlertDialog.Builder(getContext());
                builder.setMessage("确定删除?");
                builder.setTitle("提示");
                builder.setPositiveButton("确定", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if(NewsliatBeans.size()>1){
                            NewsliatBeans.remove(position-1);
                        }else {
                            Toast.makeText(getContext(),"删除失败，列表数据不足！",Toast.LENGTH_SHORT).show();
                        }
                        adapter.notifyDataSetChanged();
                        Toast.makeText(getContext(),"删除成功！",Toast.LENGTH_SHORT).show();
                    }
                });
                builder.setNegativeButton("取消",null);
                builder.create().show();
                return true;
            }
        });
        return view;
    }
    private void init(){
       always.setVisibility(View.GONE);
        my_loading_frameyout.setVisibility(View.VISIBLE);
        loading.setVisibility(View.VISIBLE);
        empty.setVisibility(View.GONE);
        error.setVisibility(View.GONE);

        OkHttpClient okHttpClient = new OkHttpClient();
       // Log.d("zzbbbbbbbbbbbbbbbbb",tabBean.getTabHref());
       Request request = new Request.Builder().url("https://temp.163.com/special/00804KVA/cm_yaowen20200213.js").method("GET",null).build();

       Call call = okHttpClient.newCall(request);
        call.enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                if(getActivity()==null)
                    return;
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        loading.setVisibility(View.GONE);
                        empty.setVisibility(View.GONE);
                        error.setVisibility(View.VISIBLE);
                        always.setVisibility(View.GONE);
                        my_loading_frameyout.setVisibility(View.VISIBLE);
                    }
                });
                return;
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {

                final String body=new String(response.body().bytes(),"GBK");
               final String jsons=StringParse.parse(body);
                URLEncoder.encode(jsons, "UTF-8");
                if(getActivity()==null)
                    return;
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        NewsliatBeans = new NewsLiatBeanManager().getNewsList(jsons);
                        for(NewsListBean n:NewsliatBeans)
                        {  if(n!=null)
                            titlelist.add(n.getTitle());}
                        adapter=new FirstFragmentListAdapter(getContext(),R.layout.list_item,NewsliatBeans);
                        listView.setAdapter(adapter);
                        if(NewsliatBeans!=null){
                            always.setVisibility(View.VISIBLE);
                            my_loading_frameyout.setVisibility(View.GONE);
                            loading.setVisibility(View.GONE);
                            empty.setVisibility(View.GONE);
                            error.setVisibility(View.GONE);
                        }
                        else {
                           always.setVisibility(View.GONE);
                            my_loading_frameyout.setVisibility(View.VISIBLE);
                            loading.setVisibility(View.GONE);
                            empty.setVisibility(View.VISIBLE);
                            error.setVisibility(View.GONE);
                        }
                    }
                });
            }
        });

        smartRefreshLayout.setOnRefreshListener(new OnRefreshListener() {
            @Override
            public void onRefresh(RefreshLayout refreshlayout) {
                if(isrefresh) {
                    isrefresh = false;
                    refreshlayout.finishRefresh(2000);//加载时间2s
                    adapter.notifyDataSetChanged();
                 /*   List<TestFruit> newfruits=new ArrayList<>();
                    for (int i = 0; i < 5; i++)
                        newfruits.add(new TestFruit("新加载的数据", R.mipmap.ic_launcher));
                    newfruits.addAll(fruits);
                    fruits.removeAll(fruits);
                    fruits.addAll(newfruits);//交换数据，使新加载的数据显示在上方
                    adapter.notifyDataSetChanged(); */
                    isrefresh = true;
                }
            }
        });
        smartRefreshLayout.setOnLoadmoreListener(new OnLoadmoreListener() {
            @Override
            public void onLoadmore(RefreshLayout refreshlayout) {
                if(isload)
                {
                    isload=false;
                    refreshlayout.finishLoadmore(2000);//传入false表示加载失败
                    OkHttpClient okHttpClient = new OkHttpClient();
                    String moreURL= reptile.tools.loadmoreURL.loadmoreurl(tabBean.getTabHref());
                    Request request = new Request.Builder().url(moreURL).method("GET",null).build();

                    Call call = okHttpClient.newCall(request);
                    call.enqueue(new Callback() {
                        @Override
                        public void onFailure(Call call, IOException e) {
                            return;
                        }

                        @Override
                        public void onResponse(Call call, Response response) throws IOException {
                            final String body=new String(response.body().bytes(),"GBK");
                            final String jsons=StringParse.parse(body);
                            if(getActivity()==null)
                                return;
                            getActivity().runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    List<NewsListBean> moreList=new NewsLiatBeanManager().getNewsList(jsons);
                                    NewsliatBeans.addAll(moreList);
                                   adapter.notifyDataSetChanged();
                                }
                            });


                        }
                    });
                    isload=true;
                }
            }
        });

        final List<RoolImageBean> RoolImageBeans=configRoolImage.getImages();//传入轮播图的地址
        List<Integer> images =new ArrayList<>();
       for(RoolImageBean Rool : RoolImageBeans){
           images.add(Rool.getImageURL());
      }
        banner.setImageLoader(new FragmentImageloader());////设置banner样式
        banner.setImages(images);//设置图片集合
        banner.setDelayTime(3500);//滚动时间
        //setbannerStyle,(BannerConfig.NOT_INDICATOR)是默认样式
        banner.start();//banner设置方法全部调用完毕时最后调用

        banner.setOnBannerListener(new OnBannerListener() {//设置轮播图点击事件
          @Override
            public void OnBannerClick(int position) {
              Intent intent=new Intent(getContext(),everyDay.class);
              intent.putExtra("every_day_URL",RoolImageBeans.get(position).getImageToHref());
              startActivity(intent);
                Toast.makeText(getContext(), "position"+position, Toast.LENGTH_SHORT).show();
            }
        });


    }
}

package Fragment;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.example.zzb.secondnews.R;
import com.scwang.smartrefresh.layout.SmartRefreshLayout;
import com.scwang.smartrefresh.layout.api.RefreshLayout;
import com.scwang.smartrefresh.layout.listener.OnLoadmoreListener;
import com.scwang.smartrefresh.layout.listener.OnRefreshListener;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import Adapter.pengpairecycleadapter;
import reptile.javabean.pengpaiNewsListBean;
import reptile.Manager.pengpaiNweslistManager;
import reptile.javabean.tabBean;

import com.example.zzb.secondnews.pengpai_newscontent;
import com.example.zzb.secondnews.pengpai_videocontent;
public class pengpaifragment extends Fragment {
private tabBean tabbean;
private RecyclerView recyclerView;
private StaggeredGridLayoutManager layoutManager;
private pengpairecycleadapter adapter;
    private Boolean isload=true;
    private Boolean isrefresh=true;
    private SmartRefreshLayout smartRefreshLayout;
    private String url;
    private FrameLayout my_loading_frameyout;
    private LinearLayout loading,empty,error;
private List<pengpaiNewsListBean>  list=new ArrayList<>();;
    public void setTabbean(tabBean tabbean) {
        this.tabbean = tabbean;
        url=tabbean.getTabHref();
    }


    @Override
    public View onCreateView( LayoutInflater inflater, ViewGroup container,  Bundle savedInstanceState) {
        View view=inflater.inflate(R.layout.pengpai_fragment,container,false);
        recyclerView=view.findViewById(R.id.pengpai_recycle);
        layoutManager=new StaggeredGridLayoutManager(2,StaggeredGridLayoutManager.VERTICAL);
        smartRefreshLayout=view.findViewById(R.id.smartpengpai);
        loading=(LinearLayout)view.findViewById(R.id.loading);
        empty=(LinearLayout)view.findViewById(R.id.empty);
        error=(LinearLayout)view.findViewById(R.id.error);
        my_loading_frameyout=(FrameLayout) view.findViewById(R.id.pengpai_loding);
        recyclerView.setLayoutManager(layoutManager);
        init();
        return view;
    }
    public void init(){
        my_loading_frameyout.setVisibility(View.VISIBLE);
        loading.setVisibility(View.VISIBLE);
        empty.setVisibility(View.GONE);
        error.setVisibility(View.GONE);
        smartRefreshLayout.setOnRefreshListener(new OnRefreshListener() {
            @Override
            public void onRefresh(RefreshLayout refreshlayout) {
                if(isrefresh) {
                    isrefresh = false;
                    refreshlayout.finishRefresh(2000);//加载时间2s
                    isrefresh = true;
                }
            }
        });
        smartRefreshLayout.setOnLoadmoreListener(new OnLoadmoreListener() {
            @Override
            public void onLoadmore(RefreshLayout refreshlayout) {
                if(isload) {
                    isload = false;
                    refreshlayout.finishLoadmore(2000);//传入false表示加载失败
                }
                isload=true;
            }

        });

        OkHttpClient okHttpClient=new OkHttpClient();
        Request request=new Request.Builder().url(url).method("GET",null).build();
        Call call=okHttpClient.newCall(request);
        call.enqueue(new Callback() {
            @Override
            public void onResponse(Call call, Response response) throws IOException {
                list=new pengpaiNweslistManager().getlist(url);
                if(getActivity()==null)
                    return;
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        adapter=new pengpairecycleadapter(getContext(),list);
                        recyclerView.setAdapter(adapter);
                        adapter.setOnitemClickListener(new pengpairecycleadapter.OnitemClickListener() {
                            @Override
                            public void OnItemClick(View view, pengpaiNewsListBean newsListBean) {
                                if(url.equals("https://www.thepaper.cn/channel_26916")) {
                                    Intent intent = new Intent(getActivity(), pengpai_videocontent.class);
                                    intent.putExtra("url", newsListBean.getHref());
                                    intent.putExtra("title", newsListBean.getTitle());
                                    intent.putExtra("image", newsListBean.getImgurl());
                                    startActivity(intent);
                                }
                                else {
                                    Intent intent = new Intent(getActivity(), pengpai_newscontent.class);
                                    intent.putExtra("url", newsListBean.getHref());
                                    intent.putExtra("title", newsListBean.getTitle());
                                    intent.putExtra("image", newsListBean.getImgurl());
                                    startActivity(intent);
                                }
                            }
                        });
                        adapter.setLongClickLisenter(new pengpairecycleadapter.LongClickLisenter() {
                            @Override
                            public void LongClickLisenter(View view,final pengpaiNewsListBean newsListBean) {
                                AlertDialog.Builder builder=new AlertDialog.Builder(getContext());
                                builder.setMessage("确定删除?");
                                builder.setTitle("提示");
                                builder.setPositiveButton("确定", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        if(list.size()>1){
                                            list.remove(newsListBean);
                                        }else {
                                            Toast.makeText(getContext(),"删除失败，列表数据不足！",Toast.LENGTH_SHORT).show();
                                        }
                                        adapter.notifyDataSetChanged();
                                        Toast.makeText(getContext(),"删除了列表项："+newsListBean.getTitle(),Toast.LENGTH_SHORT).show();
                                    }
                                });
                                builder.setNegativeButton("取消",null);
                                builder.create().show();
                            }
                        });
                        if(list!=null){

                            my_loading_frameyout.setVisibility(View.GONE);
                            loading.setVisibility(View.GONE);
                            empty.setVisibility(View.GONE);
                            error.setVisibility(View.GONE);
                        }
                        else {

                            my_loading_frameyout.setVisibility(View.VISIBLE);
                            loading.setVisibility(View.GONE);
                            empty.setVisibility(View.VISIBLE);
                            error.setVisibility(View.GONE);
                        }
                    }
                });
            }

            @Override
            public void onFailure(Call call, IOException e) {
                if(getActivity()==null)
                    return;
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        loading.setVisibility(View.GONE);
                        empty.setVisibility(View.GONE);
                        error.setVisibility(View.VISIBLE);
                        my_loading_frameyout.setVisibility(View.VISIBLE);
                    }
                });
                return;
            }
        });
    }
}

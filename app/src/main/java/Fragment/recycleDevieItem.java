package Fragment;

import android.graphics.Rect;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

public class recycleDevieItem extends RecyclerView.ItemDecoration {
    private int orientation;//方向
    private final int decoration;//边距
    public recycleDevieItem(int orientation, int decoration){
      //  super();
        this.orientation=orientation;
        this.decoration=decoration;
    }

    @Override
    public void getItemOffsets( Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
        super.getItemOffsets(outRect, view, parent, state);
        final RecyclerView.LayoutManager layoutManager=parent.getLayoutManager();
        final int lastItemPostion=state.getItemCount()-1;//得到的数目比实际多一个;
        final int current=parent.getChildLayoutPosition(view);//得到当前view的位置
        if(current==-1){
            return;
        }
        if(layoutManager    instanceof   LinearLayoutManager){//判断左边对象是否为右边实例
            if(orientation==LinearLayoutManager.VERTICAL){
                outRect.set(decoration,decoration,decoration,decoration);
            }else {
                if (current==lastItemPostion){
                    outRect.set(0,0,0,0);
                }else {
                    outRect.set(0,0,decoration,0);
                }
            }
        }
    }
}

package Fragment;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.Toast;

import Adapter.video_recycle_adapter;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import reptile.javabean.videoBean;
import reptile.Manager.VideoListManager;
import reptile.Manager.videoloadmoreManager;
import com.example.zzb.secondnews.R;
import com.scwang.smartrefresh.layout.SmartRefreshLayout;
import com.scwang.smartrefresh.layout.api.RefreshLayout;
import com.scwang.smartrefresh.layout.listener.OnLoadmoreListener;
import com.scwang.smartrefresh.layout.listener.OnRefreshListener;
import com.example.zzb.secondnews.videocontent;
import Adapter.testAdapter;
import Fragment.recycleDevieItem;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class videoFragment extends Fragment {
    private RecyclerView recyclerView;
    private Boolean isload=true;
    private Boolean isrefresh=true;
    private List<videoBean> videoBeanList;
    private video_recycle_adapter adapter;
    private SmartRefreshLayout smartRefreshLayout;
    private LinearLayout loading,empty,error,always;
    private FrameLayout frameLayout;
    private String url;
    private ArrayAdapter<String> adapter1;
    private String loadURL;
    private int more=1;

    @Override
    public View onCreateView( LayoutInflater inflater,  ViewGroup container,  Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.videofragment, container, false);
       recyclerView =view.findViewById(R.id.myvideolist);
        smartRefreshLayout = view.findViewById(R.id.video_smart);
        frameLayout=view.findViewById(R.id.video_Frame);
        loadURL="https://www.vmovier.com/post/getbytab?tab=new&page=1&pagepart=2&type=0&controller=index&last_postid=58690";
        url="https://www.vmovier.com/";
        LinearLayoutManager linearLayoutManager=new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.addItemDecoration(new recycleDevieItem(LinearLayoutManager.VERTICAL,4));
        always=view.findViewById(R.id.video_line);
        loading=(LinearLayout)view.findViewById(R.id.loading);
        empty=(LinearLayout)view.findViewById(R.id.empty);
        error=(LinearLayout)view.findViewById(R.id.error);

        init();
        return view;
    }
    private void init(){
        frameLayout.setVisibility(View.VISIBLE);
        loading.setVisibility(View.VISIBLE);
        empty.setVisibility(View.GONE);
        always.setVisibility(View.GONE);
        error.setVisibility(View.GONE);
        smartRefreshLayout.setOnRefreshListener(new OnRefreshListener() {
            @Override
            public void onRefresh(RefreshLayout refreshlayout) {
                if(isrefresh) {
                    isrefresh = false;
                    refreshlayout.finishRefresh(2000);//加载时间2s
                }
            }
        });

        smartRefreshLayout.setOnLoadmoreListener(new OnLoadmoreListener() {
            @Override
            public void onLoadmore(RefreshLayout refreshlayout) {
                if(isload)
                {
                    Log.d("zzzzzzz","调用了一次加载更多");
                    if(more==2){
                        loadURL="https://www.vmovier.com/post/getbytab?tab=new&page=1&pagepart=3&type=0&controller=index&last_postid=58690";
                    }
                    isload=false;
                    refreshlayout.finishLoadmore(2000);
                    OkHttpClient okHttpClient = new OkHttpClient();
                    Request request = new Request.Builder().url(loadURL).method("GET",null).build();
                    Call call = okHttpClient.newCall(request);
                    call.enqueue(new Callback() {
                        @Override
                        public void onFailure(Call call, IOException e) {
                            if(getActivity()==null)
                                return;
                            getActivity().runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    loading.setVisibility(View.GONE);
                                    empty.setVisibility(View.GONE);
                                    error.setVisibility(View.VISIBLE);
                                    always.setVisibility(View.GONE);
                                    frameLayout.setVisibility(View.VISIBLE);
                                }
                            });

                            return;
                        }

                        @Override
                        public void onResponse(Call call, Response response) throws IOException {
                          final  String body=new String(response.body().string());
                            if(getActivity()==null)
                                return;
                            getActivity().runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    List<videoBean> newList=new videoloadmoreManager().getmore(body);
                                    videoBeanList.addAll(newList);
                                    adapter.notifyDataSetChanged();
                                    more++;
                                }
                            });
                        }
                    });
                    isload=true;
                    if(more==3){
                        isload=false;
                    }
                }
            }
        });

        OkHttpClient okHttpClient=new OkHttpClient();
        final Request request = new Request.Builder().url(url).method("GET",null).build();
        Call call=okHttpClient.newCall(request);
        call.enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                Log.d("zzzzzzzz","6666666666");
                return;
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                videoBeanList=new VideoListManager().getVideoList(url);
                for(videoBean v:videoBeanList){
                    Log.d("zzzzzzzz",v.getTitle());
                }
                if(getActivity()==null)
                    return;
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        adapter=new video_recycle_adapter(getContext(),videoBeanList);
                        adapter.setOnitemClickListener(new video_recycle_adapter.OnitemClickListener() {
                            @Override
                            public void OnItemClick(View view, videoBean videoBean) {
                                Intent intent=new Intent(getContext(),videocontent.class);
                               intent.putExtra("href",videoBean.getPlayUrl());
                                startActivity(intent);
                                Toast.makeText(getContext(),videoBean.getPlayUrl(),Toast.LENGTH_SHORT).show();
                            }
                        });
                        adapter.setLongClickLisenter(new video_recycle_adapter.LongClickLisenter() {
                            @Override
                            public void LongClickLisenter(View view, videoBean videoBean) {
                                AlertDialog.Builder builder=new AlertDialog.Builder(getContext());
                                builder.setMessage("确定删除?");
                                builder.setTitle("提示");
                                builder.setPositiveButton("确定", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        if(videoBeanList.size()>1){
                                            videoBeanList.remove(videoBean);
                                        }else {
                                            Toast.makeText(getContext(),"删除失败，列表数据不足！",Toast.LENGTH_SHORT).show();
                                        }
                                        adapter.notifyDataSetChanged();
                                        Toast.makeText(getContext(),"删除了列表项："+videoBean.getTitle(),Toast.LENGTH_SHORT).show();
                                    }
                                });
                                builder.setNegativeButton("取消",null);
                                builder.create().show();
                            }
                        });
                        recyclerView.setAdapter(adapter);
                        if(videoBeanList!=null){
                            always.setVisibility(View.VISIBLE);
                            frameLayout.setVisibility(View.GONE);
                            loading.setVisibility(View.GONE);
                            empty.setVisibility(View.GONE);
                            error.setVisibility(View.GONE);
                        }
                        else {
                            always.setVisibility(View.GONE);
                            frameLayout.setVisibility(View.VISIBLE);
                            loading.setVisibility(View.GONE);
                            empty.setVisibility(View.VISIBLE);
                            error.setVisibility(View.GONE);
                        }
                    }
                });
            }
        });
    }
}

package Fragment;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.Toast;

import Adapter.myrecycleAdapter;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import reptile.javabean.NewsListBean;
import reptile.Manager.NewsLiatBeanManager;
import reptile.tools.StringParse;

import com.example.zzb.secondnews.NewsContent;
import com.example.zzb.secondnews.R;
import com.scwang.smartrefresh.layout.SmartRefreshLayout;
import com.scwang.smartrefresh.layout.api.RefreshLayout;
import com.scwang.smartrefresh.layout.listener.OnLoadmoreListener;
import com.scwang.smartrefresh.layout.listener.OnRefreshListener;

import java.io.IOException;
import java.net.URLEncoder;
import java.util.List;

//后续的14个fragment的模板，绑定相关控件，设置信息
public class myFragment extends Fragment {
    private View view;
    private RecyclerView recyclerView;
    private reptile.javabean.tabBean tabBean;
    private Boolean isload=true;
    private Boolean isrefresh=true;
    private SmartRefreshLayout smartRefreshLayout;
    private FrameLayout my_loading_frameyout;
    private LinearLayout loading,empty,error,always;
    private List<NewsListBean> NewListBeans;
    private LinearLayoutManager layoutManager;
    private myrecycleAdapter adapter;
    private String url;
    public void setTabBean(reptile.javabean.tabBean tabBean) {
        this.tabBean = tabBean;
        url=tabBean.getTabHref();
    }

    @Override
    public View onCreateView( LayoutInflater inflater,  ViewGroup container,  Bundle savedInstanceState) {
//绑定各个控件
        View view= inflater.inflate(R.layout.list_fragment,container,false);
        recyclerView=view.findViewById(R.id.myrecycle);
        always=(LinearLayout) view.findViewById(R.id.listpage_line);
        loading=(LinearLayout)view.findViewById(R.id.loading);
        empty=(LinearLayout)view.findViewById(R.id.empty);
        error=(LinearLayout)view.findViewById(R.id.error);
        layoutManager=new LinearLayoutManager(getContext());
        smartRefreshLayout=(SmartRefreshLayout)view.findViewById(R.id.smart2);
        my_loading_frameyout=(FrameLayout) view.findViewById(R.id.list_frame_loading);
        init();
        return view;
    }
    public void init(){
//加载界面
        always.setVisibility(View.GONE);
        my_loading_frameyout.setVisibility(View.VISIBLE);
        loading.setVisibility(View.VISIBLE);
        empty.setVisibility(View.GONE);
        error.setVisibility(View.GONE);

        smartRefreshLayout.setOnRefreshListener(new OnRefreshListener() {
            @Override
            public void onRefresh(RefreshLayout refreshlayout) {
                if(isrefresh) {
                    isrefresh = false;
                    refreshlayout.finishRefresh(2000);//加载时间2s
                 /*   List<TestFruit> newfruits=new ArrayList<>();
                    for (int i = 0; i < 5; i++)
                        newfruits.add(new TestFruit("新加载的数据", R.mipmap.ic_launcher));
                    newfruits.addAll(fruits);
                    fruits.removeAll(fruits);
                    fruits.addAll(newfruits);//交换数据，使新加载的数据显示在上方
                    adapter.notifyDataSetChanged(); */
                    isrefresh = true;
                }
            }
        });
         smartRefreshLayout.setOnLoadmoreListener(new OnLoadmoreListener() {
            @Override
            public void onLoadmore(RefreshLayout refreshlayout) {
                if(isload)
                {
                    isload=false;
                    refreshlayout.finishLoadmore(2000);//传入false表示加载失败
                    OkHttpClient okHttpClient = new OkHttpClient();
                    String moreURL= reptile.tools.loadmoreURL.loadmoreurl(tabBean.getTabHref());
                    Request request = new Request.Builder().url(moreURL).method("GET",null).build();

                    Call call = okHttpClient.newCall(request);
                    call.enqueue(new Callback() {
                        @Override
                        public void onFailure(Call call, IOException e) {
                            return;
                        }

                        @Override
                        public void onResponse(Call call, Response response) throws IOException {
                            final String body=new String(response.body().bytes(),"GBK");
                            final String jsons=StringParse.parse(body);
                            if(getActivity()==null)
                                return;
                            getActivity().runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    List<NewsListBean> moreList=new NewsLiatBeanManager().getNewsList(jsons);
                                    NewListBeans.addAll(moreList);
                                   adapter.notifyDataSetChanged();
                                }
                            });


                        }
                    });
                    isload=true;
                }
            }
        });
        OkHttpClient okHttpClient = new OkHttpClient();

//        Log.d("zzzzzzzzzzz",url);
        Request request = new Request.Builder().url(url).method("GET",null).build();
        Call call=okHttpClient.newCall(request);
        call.enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {

                if(getActivity()==null)
                    return;
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        loading.setVisibility(View.GONE);
                        empty.setVisibility(View.GONE);
                        error.setVisibility(View.VISIBLE);
                        always.setVisibility(View.GONE);
                        my_loading_frameyout.setVisibility(View.VISIBLE);
                    }
                });

                return;
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                final String body=new String(response.body().bytes(),"GBK");
                final String jsons=StringParse.parse(body);
                URLEncoder.encode(jsons, "UTF-8");
                if(getActivity()==null)
                    return;
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        NewListBeans=new NewsLiatBeanManager().getNewsList(jsons);
                        recyclerView.setItemAnimator(new DefaultItemAnimator());
                        recyclerView.addItemDecoration(new recycleDevieItem(LinearLayoutManager.VERTICAL,2));
                        recyclerView.setLayoutManager(layoutManager);
                        adapter=new myrecycleAdapter(getContext(),NewListBeans);
                        //绑定recycle的子项点击事件
                        adapter.setOnitemClickListener(new myrecycleAdapter.OnitemClickListener() {

                            @Override
                            public void OnItemClick(View view, NewsListBean newsListBean) {

                                Intent intent=new Intent(getActivity(),NewsContent.class);
                                intent.putExtra("url",newsListBean.getHref());
                                intent.putExtra("title",newsListBean.getTitle());
                                intent.putExtra("image",newsListBean.getImgurl());
                                intent.putExtra("commenturl",newsListBean.getCommentURL());
                                startActivity(intent);

                                Toast.makeText(getActivity(),"点击了"+newsListBean.getHref(),Toast.LENGTH_SHORT).show();
                            }
                        });
                        adapter.setLongClickLisenter(new myrecycleAdapter.LongClickLisenter() {
                            @Override
                            public void LongClickLisenter(View view, NewsListBean newsListBean) {
                                AlertDialog.Builder builder=new AlertDialog.Builder(getContext());
                                builder.setMessage("确定删除?");
                                builder.setTitle("提示");
                                builder.setPositiveButton("确定", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        if(NewListBeans.size()>1){
                                            NewListBeans.remove(newsListBean);
                                        }else {
                                            Toast.makeText(getContext(),"删除失败，列表数据不足！",Toast.LENGTH_SHORT).show();
                                        }
                                        adapter.notifyDataSetChanged();
                                        Toast.makeText(getContext(),"删除了列表项："+newsListBean.getTitle(),Toast.LENGTH_SHORT).show();
                                    }
                                });
                                builder.setNegativeButton("取消",null);
                                builder.create().show();

                            }
                        });
                        recyclerView.setAdapter(adapter);
                        if(NewListBeans!=null){
                            always.setVisibility(View.VISIBLE);
                            my_loading_frameyout.setVisibility(View.GONE);
                            loading.setVisibility(View.GONE);
                            empty.setVisibility(View.GONE);
                            error.setVisibility(View.GONE);
                        }
                        else {
                            always.setVisibility(View.GONE);
                            my_loading_frameyout.setVisibility(View.VISIBLE);
                            loading.setVisibility(View.GONE);
                            empty.setVisibility(View.VISIBLE);
                            error.setVisibility(View.GONE);
                        }
                    }
                });
            }
        });

    }
}

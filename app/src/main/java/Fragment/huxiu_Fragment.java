package Fragment;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.zzb.secondnews.R;
import com.example.zzb.secondnews.pengpai_newscontent;
import com.scwang.smartrefresh.layout.SmartRefreshLayout;
import com.scwang.smartrefresh.layout.api.RefreshLayout;
import com.scwang.smartrefresh.layout.listener.OnLoadmoreListener;
import com.scwang.smartrefresh.layout.listener.OnRefreshListener;


import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import Adapter.huxiu_listAdapter;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import reptile.Manager.huxiu_newslistManager;
import reptile.javabean.NewsListBean;
import reptile.javabean.tabBean;

public class huxiu_Fragment extends Fragment {
  private tabBean tab;
    private String url;
    private ListView listView;
    private Boolean isload=true;
    private Boolean isrefresh=true;
    private SmartRefreshLayout smartRefreshLayout;
    private FrameLayout my_loading_frameyout;
    private LinearLayout loading,empty,error;
    private TextView name;
    private List<NewsListBean>  list=new ArrayList<>();
    private huxiu_listAdapter adapter;
    private View headerView;
    public void setTab(tabBean tab) {
        this.tab = tab;
        url=tab.getTabHref();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {
        View view=inflater.inflate(R.layout.huxiu_fragment,container,false);
        listView=view.findViewById(R.id.huxiu_list);
        headerView=inflater.inflate(R.layout.huxiu_headview,null);
        smartRefreshLayout=view.findViewById(R.id.smarthuxiu);
        my_loading_frameyout=view.findViewById(R.id.huxiu_loding);
        loading=(LinearLayout)view.findViewById(R.id.loading);
        empty=(LinearLayout)view.findViewById(R.id.empty);
        error=(LinearLayout)view.findViewById(R.id.error);
        name=headerView.findViewById(R.id.tab_name);
        listView.addHeaderView(headerView);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                NewsListBean newsListBean=list.get(position);
                Intent intent = new Intent(getActivity(), pengpai_newscontent.class);
                intent.putExtra("url", newsListBean.getHref());
                startActivity(intent);
            }
        });
        listView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
              final   NewsListBean newsListBean=list.get(position);
                AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                builder.setMessage("确定删除?");
                builder.setTitle("提示");
                builder.setPositiveButton("确定", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if (list.size() > 1) {
                            list.remove(newsListBean);
                        } else {
                            Toast.makeText(getContext(), "删除失败，列表数据不足！", Toast.LENGTH_SHORT).show();
                        }
                        adapter.notifyDataSetChanged();
                        Toast.makeText(getContext(), "删除了列表项：" + newsListBean.getTitle(), Toast.LENGTH_SHORT).show();
                    }
                });
                builder.setNegativeButton("取消", null);
                builder.create().show();
                return false;
            }
        });
        name.setText(tab.getTabName());
        init();
        return view;
    }
    private void init(){
        my_loading_frameyout.setVisibility(View.VISIBLE);
        loading.setVisibility(View.VISIBLE);
        empty.setVisibility(View.GONE);
        error.setVisibility(View.GONE);
        smartRefreshLayout.setOnRefreshListener(new OnRefreshListener() {
            @Override
            public void onRefresh(RefreshLayout refreshlayout) {
                if(isrefresh) {
                    isrefresh = false;
                    refreshlayout.finishRefresh(2000);//加载时间2s
                    isrefresh = true;
                }
            }
        });
        smartRefreshLayout.setOnLoadmoreListener(new OnLoadmoreListener() {
            @Override
            public void onLoadmore(RefreshLayout refreshlayout) {
                if(isload) {
                    isload = false;
                    refreshlayout.finishLoadmore(2000);//传入false表示加载失败
                }
                isload=true;
            }

        });
        OkHttpClient okHttpClient=new OkHttpClient();
        Request request = new Request.Builder().url(url).method("GET", null).build();
        Call call = okHttpClient.newCall(request);
        call.enqueue(new Callback() {
            @Override
            public void onResponse(Call call, Response response) throws IOException {
                list=new huxiu_newslistManager().getlist(url);
                if(list==null)
                    return;
                if(getActivity()==null)
                    return;
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        for(NewsListBean b:list)
                            Log.d("zzzzzzz",b.getTitle());
                        adapter=new huxiu_listAdapter(getContext(),R.layout.huxiu_item,list);
                        listView.setAdapter(adapter);
                        if (list != null) {

                            my_loading_frameyout.setVisibility(View.GONE);
                            loading.setVisibility(View.GONE);
                            empty.setVisibility(View.GONE);
                            error.setVisibility(View.GONE);
                        } else {

                            my_loading_frameyout.setVisibility(View.VISIBLE);
                            loading.setVisibility(View.GONE);
                            empty.setVisibility(View.VISIBLE);
                            error.setVisibility(View.GONE);
                        }
                    }
                });
            }

            @Override
            public void onFailure(Call call, IOException e) {
                if (getActivity() == null)
                    return;
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        loading.setVisibility(View.GONE);
                        empty.setVisibility(View.GONE);
                        error.setVisibility(View.VISIBLE);
                        my_loading_frameyout.setVisibility(View.VISIBLE);
                    }
                });
                return;
            }
        });
    }
}

package com.example.zzb.secondnews;

import android.content.ContentValues;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.hyphenate.chat.EMClient;
import com.hyphenate.exceptions.HyphenateException;

import cn.bmob.v3.Bmob;
import cn.bmob.v3.BmobUser;
import cn.bmob.v3.exception.BmobException;
import cn.bmob.v3.listener.SaveListener;
import database.MydatabaseHelper;
public class register extends AppCompatActivity {
    private Button zhuce;
    private EditText name,psw,configpsw;
    private MydatabaseHelper mydatabaseHelper;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        zhuce=findViewById(R.id.myzhuce);
        name=findViewById(R.id.zhucename);
        psw=findViewById(R.id.zhucepsw);
        configpsw=findViewById(R.id.configpsw);
        mydatabaseHelper=login.datahelper;
        Bmob.initialize(register.this,"77c5c5f2c801b0fd46c20a07a6e1b90c");
        zhuce.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d("密码框输入了：",psw.getText().toString());
                Log.d("确认密码框输入了：",psw.getText().toString());
                if(name.getText().toString().trim().length()==0){
                    Toast.makeText(register.this,"用户名不能为空！",Toast.LENGTH_SHORT).show();
                    return;
                }
                if(psw.getText().toString().trim().length()==0){
                    Toast.makeText(register.this,"请输入密码！",Toast.LENGTH_SHORT).show();
                    return;
                }
                if(configpsw.getText().toString().trim().length()==0){
                    Toast.makeText(register.this,"请输入确认密码！",Toast.LENGTH_SHORT).show();
                    return;
                }
                if(!(psw.getText().toString().equals(configpsw.getText().toString()))){
                    Toast.makeText(register.this,"两次输入的密码不同，请重新输入！",Toast.LENGTH_SHORT).show();
                    return;
                }
                if((psw.getText().toString().equals(configpsw.getText().toString()))&&name.getText().toString().trim().length()>0){
                    BmobUser p1=new BmobUser();
                    p1.setUsername(name.getText().toString().trim());
                    p1.setPassword(psw.getText().toString().trim());
                    p1.signUp(new SaveListener<BmobUser>() {

                        @Override
                        public void done(BmobUser bmobUser, BmobException e) {
                            if(e==null){
                                new Thread(new Runnable() {
                                    @Override
                                    public void run() {
                                        try {
                                            Log.d("zzzzzzzzzz","test   successful");
                                            EMClient.getInstance().createAccount(name.getText().toString(),psw.getText().toString());
                                        } catch (HyphenateException e1) {
                                            e1.printStackTrace();
                                        }
                                    }
                                }).start();
                                Toast.makeText(register.this,"注册成功请登录。",Toast.LENGTH_SHORT).show();

                                finish();
                            }else {
                                Toast.makeText(register.this,"注册失败请重试。",Toast.LENGTH_SHORT).show();
                            }
                        }
                    });

                }
            }
        });
    }
}

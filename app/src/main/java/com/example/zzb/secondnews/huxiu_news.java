package com.example.zzb.secondnews;

import android.content.Intent;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import Adapter.fragmentAdapter;
import Adapter.huxiu_listAdapter;
import reptile.javabean.config_huxiu_url;
import reptile.javabean.tabBean;
import Fragment.huxiu_Fragment;

public class huxiu_news extends AppCompatActivity {
    private ViewPager viewPager;
    private TabLayout tabLayout;
    private huxiu_listAdapter adapter;
    private fragmentAdapter fragmentAdapter;
    private long keyname=0;
    private FloatingActionButton fab,fab_wangyi,fab_pengpai,fab_huxiu;
    private Boolean isgone=true;
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK
                && event.getAction() == KeyEvent.ACTION_DOWN) {
            if ((System.currentTimeMillis() - keyname) > 2000) {
                //弹出提示，可以有多种方式
                Toast.makeText(getApplicationContext(), "再按一次退出程序", Toast.LENGTH_SHORT).show();
                keyname = System.currentTimeMillis();
            } else {
                finish();
            }
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_huxiu_news);
        viewPager=findViewById(R.id.huxiuviewpager);
        tabLayout=findViewById(R.id.huxiu_tab_layout);
        fab=findViewById(R.id.fab);
        fab_wangyi=findViewById(R.id.fab_wangyi);
        fab_pengpai=findViewById(R.id.fab_pengpai);
        fab_huxiu=findViewById(R.id.fab_huxiu);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(isgone){
                    fab_pengpai.show();
                    fab_wangyi.show();
                    fab_huxiu.show();
                    fab_pengpai.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            startActivity(new Intent(huxiu_news.this,pengpaiNews.class));
                            finish();
                        }
                    });
                    fab_wangyi.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            startActivity(new Intent(huxiu_news.this,MainActivity.class));
                            finish();
                        }
                    });
                    fab_huxiu.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Toast.makeText(huxiu_news.this,"已经在虎嗅新闻，无需点击",Toast.LENGTH_SHORT).show();
                        }
                    });
                    isgone=false;
                }
                else {
                    fab_wangyi.hide();
                    fab_pengpai.hide();
                    fab_huxiu.hide();
                    isgone=true;

                }
            }
        });
        List<tabBean> tabs=config_huxiu_url.getlist();
        for(int k=0;k<tabs.size();k++){//设置tab的标题
            tabLayout.addTab(tabLayout.newTab().setText(tabs.get(k).getTabName()));
        }
        List<Fragment> fragments=new ArrayList<Fragment>();
        for(int i=0;i<tabs.size();i++){
            huxiu_Fragment huxiu_fragment=new huxiu_Fragment();
            huxiu_fragment.setTab(tabs.get(i));
            fragments.add(huxiu_fragment);
        }
        fragmentAdapter=new fragmentAdapter(getSupportFragmentManager(),fragments,tabs);
        viewPager.setAdapter(fragmentAdapter);
        tabLayout.setupWithViewPager(viewPager);//绑定tab和viewPager
    }
}

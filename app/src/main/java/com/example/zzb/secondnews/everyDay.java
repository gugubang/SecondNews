package com.example.zzb.secondnews;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;

public class everyDay extends AppCompatActivity {
private WebView webView;
private LinearLayout loading;
private boolean isfinsh=false;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_every_day);
        webView=findViewById(R.id.every_day);
        loading =(LinearLayout) findViewById(R.id.web_loading);
        WebSettings webSettings=webView.getSettings();
        webSettings.setJavaScriptEnabled(true);
        webSettings.setBuiltInZoomControls(true);//内置变焦机制
        webSettings.setUseWideViewPort(true);// 这个很关键，设定宽度
        webSettings.setLoadWithOverviewMode(true);
        webView.setWebViewClient(new WebViewClient(){
            @Override
            public void onPageFinished(WebView view, String url) {
                loading.setVisibility(View.GONE);
                super.onPageFinished(view, url);
            }
        });
        Intent intent=getIntent();
        String URL=intent.getStringExtra("every_day_URL");
        webView.loadUrl(URL);
    }
public void init(){

}

}

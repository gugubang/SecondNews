package com.example.zzb.secondnews;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;

import reptile.javabean.contentBean;
import reptile.Manager.contentBeanManager;
import java.io.IOException;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class NewsContent extends AppCompatActivity  implements View.OnClickListener{
    private Button back;
    private Button light;
    private Button settingadd,settingjian;
    private Button share;
    private ImageView img;
    private TextView title,root,author,otitle;
    private Boolean islight=true;
    private ScrollView myscroll;
    private contentBean contentBean;
    private FrameLayout frameLayout;
    private LinearLayout loading,empty,error,always;
    private WebView contentWebview,commentWebview;
    private int zoom=100;
    private WebSettings webSettings,webSettings2;
    private String imgurl,lookimgurl,shareURL,commenturl;
    String javascript =  "javascript:function getClass(parent,sClass){" +
            "var aEle=parent.getElementsByTagName('div');" +
            "var aResult=[];" +
            "var i=0;" +
            "for(i<0;i<aEle.length;i++){" +
            "if(aEle[i].className==sClass){" +
            "aResult.push(aEle[i]);" +
            "}" +
            "};" +
            "return aResult;}" +
            "function hideOther() {" +
            "getClass(document,'nav-bar')[0].style.display='none';" +
            "getClass(document,'wrapper orig-post')[0].style.display='none';"+
            "getClass(document,'tie-head')[0].style.display='none';"+
            "getClass(document,'tie-foot')[0].style.display='none';"+
            "getClass(document,'side-bar')[0].style.display='none';"+
            "getClass(document,'wrapper copyright')[0].style.display='none';"+
            "getClass(document,'top-banner')[0].style.display='none';}";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_news_content);
        back=findViewById(R.id.back);
        light=findViewById(R.id.light);
        settingadd=findViewById(R.id.settingadd);
        settingjian=findViewById(R.id.settingjian);
        share=findViewById(R.id.share);
        frameLayout=findViewById(R.id.contentframe);
        title=findViewById(R.id.contenttitle);
        myscroll=findViewById(R.id.myscroll);
        author=findViewById(R.id.author);
        img=findViewById(R.id.contentimg);
        root=findViewById(R.id.root);
        otitle=findViewById(R.id.otitle);
        contentWebview = findViewById(R.id.contentview);
        commentWebview=findViewById(R.id.comment);
        loading=(LinearLayout)findViewById(R.id.loading);
        empty=(LinearLayout)findViewById(R.id.empty);
        error=(LinearLayout)findViewById(R.id.error);
        always=(LinearLayout)findViewById(R.id.myalways);

        initdata();

    }
    public void initdata(){
        frameLayout.setVisibility(View.VISIBLE);
        loading.setVisibility(View.VISIBLE);
        empty.setVisibility(View.GONE);
        error.setVisibility(View.GONE);
        Intent intent=getIntent();
        imgurl=intent.getStringExtra("image");
        final String url=intent.getStringExtra("url");
        shareURL=url;
        //title.setText(url);
        back.setOnClickListener(this);
        light.setOnClickListener(this);
        settingadd.setOnClickListener(this);
        settingjian.setOnClickListener(this);
        share.setOnClickListener(this);
        contentWebview.setWebChromeClient(new WebChromeClient());
        contentWebview.setWebViewClient(new WebViewClient());
        webSettings = contentWebview.getSettings();
        contentWebview.setBackgroundColor(Color.parseColor("#f8fcff"));

        commentWebview.setWebChromeClient(new WebChromeClient(){

            @Override
            public void onProgressChanged(WebView view, int newProgress) {
                if(newProgress<100)
                {
                    frameLayout.setVisibility(View.VISIBLE);
                    loading.setVisibility(View.VISIBLE);
                    empty.setVisibility(View.GONE);
                    error.setVisibility(View.GONE);
                }
                else {
                    frameLayout.setVisibility(View.GONE);
                    loading.setVisibility(View.GONE);
                    empty.setVisibility(View.GONE);
                    error.setVisibility(View.GONE);
                }
                super.onProgressChanged(view, newProgress);
            }
        });
        commentWebview.setWebViewClient(new WebViewClient(){
            @Override
            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(view, url);
                //创建方法
                view.loadUrl(javascript);

                //加载方法
                view.loadUrl("javascript:hideOther();");}
        });
        webSettings2 = commentWebview.getSettings();
        commentWebview.setBackgroundColor(Color.parseColor("#f8fcff"));
        myscroll.setBackgroundColor(Color.parseColor("#f8fcff"));
        webSettings.setJavaScriptEnabled(true);  //开启javascript
        webSettings.setDomStorageEnabled(true);  //开启DOM
        webSettings.setDefaultTextEncodingName("utf-8"); //设置编码
        webSettings.setAllowFileAccess(true);// 支持文件流
        webSettings.setBlockNetworkImage(true); //提高网页加载速度，暂时阻塞图片加载，然后网页加载好了，在进行加载图片
        webSettings.setAppCacheEnabled(true);//开启缓存机制
        webSettings2.setJavaScriptEnabled(true);
        webSettings2.setBuiltInZoomControls(true);//内置变焦机制
        webSettings2.setUseWideViewPort(true);// 这个很关键，设定宽度
        webSettings2.setLoadWithOverviewMode(true);
        webSettings2.setUseWideViewPort(true);

        OkHttpClient okHttpClient = new OkHttpClient();
        final Request request = new Request.Builder().url(url).method("GET",null).build();
        Call call = okHttpClient.newCall(request);
        call.enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                frameLayout.setVisibility(View.VISIBLE);
                loading.setVisibility(View.GONE);
                empty.setVisibility(View.GONE);
                error.setVisibility(View.VISIBLE);
                return;
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                contentBean=new contentBeanManager().getContent(url);
               runOnUiThread(new Runnable() {
                   @Override
                   public void run() {

                       Intent intent=getIntent();
                       commenturl=intent.getStringExtra("commenturl");
                       String mytitle=intent.getStringExtra("title");
                       //Log.d("zzb",contentBean.toString());
                       title.setText(mytitle);
                       otitle.setText(contentBean.getOldtitle());
                       author.setText(contentBean.getAuthor());
                       root.setText(contentBean.getRtime());
                       if(contentBean.getImage()=="https://ss0.bdstatic.com/70cFuHSh_Q1YnxGkpoWK1HF6hhy/it/u=386535230,3956809074&fm=26&gp=0.jpg")
                       {  Glide.with(NewsContent.this).load(imgurl).into(img);
                       lookimgurl=imgurl;}
                       else {Glide.with(NewsContent.this).load(contentBean.getImage()).into(img);
                       lookimgurl=contentBean.getImage();}
                       img.setOnClickListener(new View.OnClickListener() {
                           @Override
                           public void onClick(View v) {
                               Log.d("zzzzzzzzzzzz","99999999999999");
                               Intent intent=new Intent(NewsContent.this,lookImage.class);
                                   intent.putExtra("url",lookimgurl);
                                   startActivity(intent);

                           }
                       });
                       commentWebview.loadUrl(commenturl);
                       if(contentBean.getContext().toString()!=null)
                     //  Log.d("zzzzzzzzzzz",contentBean.getContext().toString());
                       contentWebview.loadDataWithBaseURL(url,contentBean.getContext().toString(),"text/html","UTF-8","");
                       if(contentBean!=null){

                       }else {
                           frameLayout.setVisibility(View.VISIBLE);
                           loading.setVisibility(View.GONE);
                           empty.setVisibility(View.VISIBLE);
                           error.setVisibility(View.GONE);
                       }
                   }
               });

                }

        });
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.back:
                this.finish();
                break;
            case R.id.light:
                if (islight) {
                    light.setBackgroundResource(R.drawable.night);
                    contentWebview.setBackgroundColor(Color.parseColor("#808080"));
                    commentWebview.setBackgroundColor(Color.parseColor("#808080"));
                    myscroll.setBackgroundColor(Color.parseColor("#808080"));
                    islight = false;
                    break;
                } else {
                    myscroll.setBackgroundColor(Color.parseColor("#f8fcff"));
                    contentWebview.setBackgroundColor(Color.parseColor("#f8fcff"));
                    commentWebview.setBackgroundColor(Color.parseColor("#f8fcff"));
                    light.setBackgroundResource(R.drawable.light);
                    islight = true;
                    break;
                }
            case R.id.settingadd:
                if (zoom < 120)
                {zoom+=10;
                webSettings.setTextZoom(zoom);
                Toast.makeText(NewsContent.this,"你点击了setting",Toast.LENGTH_SHORT).show();
                }
                break;
            case R.id.settingjian:
                if(zoom>90)
                {zoom-=10;
                webSettings.setTextZoom(zoom);
                Toast.makeText(NewsContent.this,"你点击了setting",Toast.LENGTH_SHORT).show();
                }
                break;
            case R.id.share:
                Intent intent=new Intent(Intent.ACTION_SEND);
                intent.setType("text/plain"); //"image/*"
                intent.putExtra(Intent.EXTRA_SUBJECT,"新闻分享");
                intent.putExtra(Intent.EXTRA_TEXT, "快来看最新热文啦！"+"\n"+contentBean.getTitle()+"\n"+shareURL);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(Intent.createChooser(intent, "选择分享的软件"));
                Toast.makeText(NewsContent.this,"你点击了share",Toast.LENGTH_SHORT).show();
                break;
        }

    }
}

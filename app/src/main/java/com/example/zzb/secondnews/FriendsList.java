package com.example.zzb.secondnews;

import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import com.hyphenate.chat.EMClient;

import Adapter.friendAdapter;
import cn.bmob.v3.Bmob;
import cn.bmob.v3.BmobQuery;
import cn.bmob.v3.BmobUser;
import cn.bmob.v3.exception.BmobException;
import cn.bmob.v3.listener.FindListener;

import java.util.ArrayList;
import java.util.List;

public class FriendsList extends AppCompatActivity implements View.OnClickListener {
    private ListView friends;
    private  List<String> names=new ArrayList<>();
    private Button back,adduser;
    private friendAdapter adapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_friends_list);
        //数据库实例化
        Bmob.initialize(FriendsList.this,"77c5c5f2c801b0fd46c20a07a6e1b90c");
    BmobQuery<BmobUser>  users=new BmobQuery<>();//查询数据库所有的用户
        users.findObjects(new FindListener<BmobUser>() {
            @Override
            public void done(List<BmobUser> list, BmobException e) {
                if(e==null){
                    for(BmobUser b:list)
                    {
                        names.add(b.getUsername());
                        Log.d("zzzzz",b.getUsername());

                    }
                    adapter=new friendAdapter(FriendsList.this,R.layout.list_friend_item,names);
                    Log.d("zzzzz",names.get(1));
                    friends.setAdapter(adapter);
                }else {
                    Log.d("zzzzz","查询失败！");
                    return;
                }
            }
        });

        friends=findViewById(R.id.friendslistview);
    back=findViewById(R.id.friends_back);
    adduser=findViewById(R.id.add_friends);
    back.setOnClickListener(this);


        friends.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String name=names.get(position);
                Intent intent=new Intent(FriendsList.this,chatview.class);
                intent.putExtra("username",name);
                intent.putExtra("listcount",names.size());
                intent.putExtra("postion",position);
                Toast.makeText(FriendsList.this,"即将进入和"+name+"的聊天界面",Toast.LENGTH_SHORT).show();
                startActivity(intent);
            }
        });
    adduser.setOnClickListener(this);

    }


    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.add_friends:
                AlertDialog.Builder builder=new AlertDialog.Builder(FriendsList.this);
                builder.setTitle("请输入添加好友的用户名：");
                builder.setIcon(R.drawable.addlog);
                EditText editText=new EditText(FriendsList.this);
                builder.setView(editText);
                builder.setPositiveButton("添加", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if(editText.getText().toString().trim().length()==0){
                            Toast.makeText(FriendsList.this,"用户名不能为空，请输入。",Toast.LENGTH_SHORT).show();
                            return;
                        }
                        BmobQuery<BmobUser> users=new BmobQuery<>();
                        users.addWhereEqualTo("username",editText.getText().toString());
                        users.findObjects(new FindListener<BmobUser>() {
                            @Override
                            public void done(List<BmobUser> list, BmobException e) {
                                if(e==null){
                                    names.add(editText.getText().toString());
                                    adapter.notifyDataSetChanged();
                                    Toast.makeText(FriendsList.this,"用户存在，添加成功。",Toast.LENGTH_SHORT).show();
                                }else{
                                    Toast.makeText(FriendsList.this,"用户不存在，添加失败。",Toast.LENGTH_SHORT).show();
                                }
                            }
                        });
                        Toast.makeText(FriendsList.this,editText.getText().toString(),Toast.LENGTH_SHORT).show();
                    }
                });
                builder.setNegativeButton("取消",null);
                builder.show();
                break;
            case R.id.friends_back:
                finish();

        }
    }
}

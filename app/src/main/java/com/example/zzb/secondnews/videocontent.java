package com.example.zzb.secondnews;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.MediaController;
import android.widget.TextView;
import android.widget.VideoView;
import reptile.javabean.video_contentBean;
import reptile.Manager.video_contentManager;
import java.io.IOException;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class videocontent extends AppCompatActivity {
private WebView webView;
private TextView videotitle,videotype,videotime,videocount,videocontent;
private String url;
private FrameLayout frameLayout;
private LinearLayout loading,empty,error,always;
private video_contentBean bean;
    private MediaController mediaController;
    private Button back,share;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_videocontent);


        webView=findViewById(R.id.video_play);
        mediaController=new MediaController(this);
        back=findViewById(R.id.video_back);
        share=findViewById(R.id.video_share);
        videotitle=findViewById(R.id.vdeo_content_title);
        videotype=findViewById(R.id.videotype);
        videotime=findViewById(R.id.data_time);
        videocount=findViewById(R.id.playcount);
        videocontent=findViewById(R.id.video_content);
        loading=(LinearLayout)findViewById(R.id.loading);
        empty=(LinearLayout)findViewById(R.id.empty);
        error=(LinearLayout)findViewById(R.id.error);
        always=(LinearLayout)findViewById(R.id.videoalways);
        frameLayout=findViewById(R.id.videocontentFrame);
        WebSettings webSettings=webView.getSettings();
        webView.setWebChromeClient(new WebChromeClient());
        webView.setWebViewClient(new WebViewClient());
        init();
    }
    public void init(){
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        share.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(Intent.ACTION_SEND);
                intent.setType("text/plain"); //"image/*"
                intent.putExtra(Intent.EXTRA_SUBJECT,"视频分享");
                intent.putExtra(Intent.EXTRA_TEXT, "快来看最新视频啦！"+"\n"+bean.getTitle()+"\n"+url);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(Intent.createChooser(intent, "选择分享的软件"));
            }
        });
        frameLayout.setVisibility(View.VISIBLE);
        loading.setVisibility(View.VISIBLE);
        empty.setVisibility(View.GONE);
        error.setVisibility(View.GONE);
        OkHttpClient okHttpClient = new OkHttpClient();
        Intent intent=getIntent();
        url=intent.getStringExtra("href");
        Log.d("zzzzzzzzzz","url 是"+url);
        if(url==null)
            return;
       final Request request = new Request.Builder().url(url).method("GET",null).build();
        Call call=okHttpClient.newCall(request);
        call.enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                frameLayout.setVisibility(View.VISIBLE);
                loading.setVisibility(View.GONE);
                empty.setVisibility(View.GONE);
                error.setVisibility(View.VISIBLE);
                return;
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                bean=new video_contentManager().getcontent(url);
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        videotitle.setText(bean.getTitle());
                        videotype.setText(bean.getTags());
                        videotime.setText(bean.getDatatime());
                        videocount.setText("播放次数："+bean.getCount());
                        videocontent.setText(bean.getDescription());
                        webView.loadUrl(bean.getPlayUrl());
                        Log.d("zzzzzzzz",bean.getPlayUrl());
                        if(bean!=null)
                        { frameLayout.setVisibility(View.GONE);
                        loading.setVisibility(View.GONE);
                        empty.setVisibility(View.GONE);
                        error.setVisibility(View.GONE);}
                        else {
                            frameLayout.setVisibility(View.VISIBLE);
                            loading.setVisibility(View.GONE);
                            empty.setVisibility(View.VISIBLE);
                            error.setVisibility(View.GONE);
                        }
                    }
                });
            }
        });
    }
}

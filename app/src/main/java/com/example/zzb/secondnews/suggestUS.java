package com.example.zzb.secondnews;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class suggestUS extends AppCompatActivity {
private EditText suggestion,tel;
private Button submit,back;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_suggest_us);
        suggestion=findViewById(R.id.suggestion);
        tel=findViewById(R.id.tel);
        submit=findViewById(R.id.submit);
        back=findViewById(R.id.suggestback);
        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d("zzzzzzzzz","!!!!!!!!!!!!");
                if(tel.getText().toString().trim().length()==0)
                    Toast.makeText(suggestUS.this,"请输入电话号码!!!",Toast.LENGTH_SHORT).show();
                else {
                    suggestion.setText("");
                    tel.setText("");
                    Toast.makeText(suggestUS.this,"我们已收到您的宝贵建议，谢谢您的反馈！",Toast.LENGTH_SHORT).show();
                    finish();
                }
            }
        });
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
                Log.d("zzzzzzzzz","!!!!!!!!!!!!");
            }
        });

    }
}

package com.example.zzb.secondnews;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.Vibrator;
import android.support.v4.app.NotificationCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.hyphenate.EMMessageListener;
import com.hyphenate.chat.EMClient;
import com.hyphenate.chat.EMMessage;

import Adapter.MsgAdapter;
import cn.bmob.v3.Bmob;

import java.util.ArrayList;
import java.util.List;

public class chatview extends AppCompatActivity {
 /*   private  static List<message> mylist=new ArrayList<>();
    private  static List<message> myzzblist=new ArrayList<>();
    private  static List<message> myzzb1list=new ArrayList<>();
    private  static List<message> myzzb2list=new ArrayList<>();*/
    private static  List<List<message>>  lists=new ArrayList<>();
    //点击不同的用户存储历史信息所用的列表不同
    private EditText input;
    private Button send;
    private RecyclerView recyclerView;
    private MsgAdapter adapter;
    public  EMMessageListener  emMessageListener;
    private Vibrator vibrator;
    private TextView item;
    private NotificationManager notificationManager;
    private final String channei_id="ch_id_1";
    private final String channelname="ch_name_1";
    NotificationManager manager;
    @Override
    protected void onDestroy() {
        EMClient.getInstance().chatManager().removeMessageListener( emMessageListener);
        super.onDestroy();
    }
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if(keyCode == KeyEvent.KEYCODE_BACK){
            moveTaskToBack(true);
            //让聊天界面活动返回键不销毁，实现全局接收消息
            return false;
        }
        return super.onKeyDown(keyCode, event);
    }

    @Override
    protected void onResume() {
        EMClient.getInstance().chatManager().addMessageListener(emMessageListener);
        super.onResume();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chatview);
        manager= (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            //只在Android O之上需要渠道
            NotificationChannel notificationChannel = new NotificationChannel(channei_id,
                   channelname, NotificationManager.IMPORTANCE_HIGH);
            //如果这里用IMPORTANCE_NOENE就需要在系统的设置里面开启渠道，
            //通知才能正常弹出
            manager.createNotificationChannel(notificationChannel);
        }

        int count=getIntent().getIntExtra("listcount",1);
        final int postion=getIntent().getIntExtra("postion",0);
        for(int i=0;i<count;i++){
            lists.add(new ArrayList<>());
        }
        Bmob.initialize(chatview.this,"77c5c5f2c801b0fd46c20a07a6e1b90c");
        item=findViewById(R.id.chat_item);
        input=findViewById(R.id.input_text);
        send=findViewById(R.id.send);
        recyclerView=findViewById(R.id.messagerecycle);
        LinearLayoutManager linearLayoutManager=new LinearLayoutManager(this);
        recyclerView.setLayoutManager(linearLayoutManager);
        final String name=getIntent().getStringExtra("username");
        item.setText("和"+name+"聊天中");
        //List<message> lastlist=namelist(name);
        emMessageListener=new EMMessageListener() {
            @Override
            public void onMessageReceived(List<EMMessage> list) {
                String result = list.get(0).getBody().toString();
                String msgReceived = result.substring(5, result.length() - 1);
                final message msg = new message(msgReceived, message.TYPE_RECEIVED);
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        NotificationCompat.Builder builder= new NotificationCompat.Builder(chatview.this,channei_id);
                        builder.setSmallIcon(R.drawable.touxiang01)
                                .setContentTitle(name)
                                .setContentText(msg.getContent())
                                .setAutoCancel(true);
                        Intent intent =new Intent (chatview.this,BroadCastReceiver.class);
                        PendingIntent pendingIntent =PendingIntent.getBroadcast(chatview.this, 0, intent, 0);
                        builder.setContentIntent(pendingIntent);
                        manager.notify(1, builder.build());
                        //通过广播接收器，实现点击通知跳转到通话列表
                        vibrator=(Vibrator) getSystemService(Service.VIBRATOR_SERVICE);
                        vibrator.vibrate(1000);
                        lists.get(postion).add(msg);
                        adapter.notifyDataSetChanged();
                        recyclerView.scrollToPosition(lists.get(postion).size() - 1);
                    }
                });
            }
            @Override
            public void onCmdMessageReceived(List<EMMessage> list) {

            }

            @Override
            public void onMessageRead(List<EMMessage> list) {

            }

            @Override
            public void onMessageDelivered(List<EMMessage> list) {

            }

            @Override
            public void onMessageRecalled(List<EMMessage> list) {

            }

            @Override
            public void onMessageChanged(EMMessage emMessage, Object o) {

            }
        };


        adapter=new MsgAdapter(lists.get(postion));
        recyclerView.setAdapter(adapter);

        send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String msg=input.getText().toString();
                if(!msg.equals("")){
                    message message=new message(msg,com.example.zzb.secondnews.message.TYPE_SEND);
                    lists.get(postion).add(message);
                    EMMessage sendmessage=EMMessage.createTxtSendMessage(msg,name);
                    EMClient.getInstance().chatManager().sendMessage(sendmessage);
                    adapter.notifyItemInserted(lists.get(postion).size()-1);
                    recyclerView.scrollToPosition(lists.get(postion).size()-1);
                    input.setText("");

                }
            }
        });
    }
}

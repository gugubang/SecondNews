package com.example.zzb.secondnews;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.app.ListFragment;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.hyphenate.EMCallBack;
import com.hyphenate.chat.EMClient;

import Adapter.*;
import reptile.javabean.*;
import Fragment.*;
import java.util.ArrayList;
import java.util.List;
public class MainActivity extends AppCompatActivity {
    private DrawerLayout drawerLayout;
    private Toolbar toolbar;
    private ActionBar actionBar;
   private ViewPager viewPager;
   private TabLayout tabLayout;
    private fragmentAdapter fragmentAdapter;
    private ImageView loadingdata;
    private NavigationView navView;
    private TextView user_name;
    private long keyname=0;
    private FloatingActionButton fab,fab_wangyi,fab_pengpai,fab_huxiu;
    private Boolean isgone=true;
    private View headview;
    private TextView head_username;
    private String getusername;
    public boolean onCreateOptionsMenu(Menu menu){ //标题栏的布局加载，菜单布局
        getMenuInflater().inflate(R.menu.toolbar,menu);
        return true;
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK
                         && event.getAction() == KeyEvent.ACTION_DOWN) {
                   if ((System.currentTimeMillis() - keyname) > 2000) {
                         //弹出提示，可以有多种方式
                            Toast.makeText(getApplicationContext(), "再按一次退出程序", Toast.LENGTH_SHORT).show();
                          keyname = System.currentTimeMillis();
                      } else {
                             finish();
                       }
                       return true;
                  }
        return super.onKeyDown(keyCode, event);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case android.R.id.home://homeasup按钮的ID永远是android.R.id.home
                drawerLayout.openDrawer(GravityCompat.START);//滑动菜单展示，
                break;
            case R.id.search:
                startActivity(new Intent(MainActivity.this,searchNews.class));
                Toast.makeText(this,"66666",Toast.LENGTH_SHORT).show();
                break;
        }
        return true;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_aitivity);
        user_name=findViewById(R.id.user_name);
        initView();
        if(actionBar!=null){
            actionBar.setDisplayHomeAsUpEnabled(true);//显示导航按钮
            actionBar.setHomeAsUpIndicator(R.drawable.user3);//导航按钮图标
        }
        initView();//初始化一系列控件
        initData();//初始化数据
        head_username.setText(login.pre.getString("username","游客"));
    }
    public void initView(){
        fab=findViewById(R.id.fab);
        fab_wangyi=findViewById(R.id.fab_wangyi);
        fab_pengpai=findViewById(R.id.fab_pengpai);
        fab_huxiu=findViewById(R.id.fab_huxiu);
        toolbar=(Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("");
        navView=findViewById(R.id.nav_view);
        headview=navView.getHeaderView(0);
        head_username=headview.findViewById(R.id.user_name);
        setSupportActionBar(toolbar);//将toolbar实例传入，使用toolbar且外观功能与actionbar一致
        drawerLayout=(DrawerLayout)findViewById(R.id.drawer_layout);
        actionBar=getSupportActionBar();//
        viewPager=(ViewPager)findViewById(R.id.viewpager);
    }

    public void initData(){
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(isgone){
                    fab_pengpai.show();
                    fab_wangyi.show();
                    fab_huxiu.show();
                    fab_pengpai.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            startActivity(new Intent(MainActivity.this,pengpaiNews.class));
                            finish();
                        }
                    });
                    fab_wangyi.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Toast.makeText(MainActivity.this,"已经在网易新闻，无需点击",Toast.LENGTH_SHORT).show();
                        }
                    });
                    fab_huxiu.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            startActivity(new Intent(MainActivity.this,huxiu_news.class));
                            finish();
                        }
                    });
                    isgone=false;
                }
                else {
                    fab_wangyi.hide();
                    fab_pengpai.hide();
                    fab_huxiu.hide();
                    isgone=true;

                }
            }
        });
        navView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected( MenuItem menuItem) {
                switch (menuItem.getItemId()){
                    case R.id.user_information:
                        //startActivity(new Intent(MainActivity.this,huxiu_news.class));
                        Toast.makeText(MainActivity.this,"用户名:"+login.pre.getString("username","游客")+"\n"+"普通会员",Toast.LENGTH_LONG).show();
                        break;
                    case R.id.my_friends:
                        startActivity(new Intent(MainActivity.this,FriendsList.class));
                        Toast.makeText(MainActivity.this,"进入好友列表",Toast.LENGTH_SHORT).show();
                        break;
                    case R.id.suggest:
                        Intent intent_suggest=new Intent(MainActivity.this,suggestUS.class);
                        startActivity(intent_suggest);
                        break;
                    case R.id.aboutus:
                        Intent intent_about=new Intent(MainActivity.this,aboutUS.class);
                        startActivity(intent_about);
                        break;
                    case R.id.setting:
                        EMClient.getInstance().logout(true, new EMCallBack() {
                            @Override
                            public void onSuccess() {
                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        Toast.makeText(MainActivity.this,"退出成功请登录。",Toast.LENGTH_SHORT).show();
                                        startActivity(new Intent(MainActivity.this,login.class));
                                    }
                                });
                            }

                            @Override
                            public void onError(int i, String s) {
                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        Toast.makeText(MainActivity.this,"退出失败。",Toast.LENGTH_SHORT).show();
                                        finish();
                                    }
                                });
                            }

                            @Override
                            public void onProgress(int i, String s) {

                            }
                        });
                        finish();
                        Toast.makeText(MainActivity.this,"感谢您的陪伴，再见！",Toast.LENGTH_SHORT).show();
                        break;
                }

               return true;
            }
        });
        tabLayout = findViewById(R.id.tab_layout);
        List<tabBean> tabs=configTabBean.getTabBean();
        for(int k=0;k<tabs.size();k++){//设置tab的标题
    tabLayout.addTab(tabLayout.newTab().setText(tabs.get(k).getTabName()));
        }
        List<Fragment> fragments=new ArrayList<Fragment>();
        for(int j=0;j<tabs.size();j++){
            if(j==0){
                FirstFragment firstFragment=new FirstFragment();
                firstFragment.setTabBean(tabs.get(0));
                fragments.add(firstFragment);
            }
           else {
                if(j==1){
                    videoFragment videoFragment=new videoFragment();
                    fragments.add(videoFragment);
                }
                else {
                myFragment myFragment =new myFragment();
                myFragment.setTabBean(tabs.get(j));
           //myFragment.setTitle(tabs.get(j).getTabHref());
           fragments.add(myFragment);
                }
            }
        }
        //通过适配器传入fragment到viewPager中
        fragmentAdapter=new fragmentAdapter(getSupportFragmentManager(),fragments,tabs);
        viewPager.setAdapter(fragmentAdapter);
        tabLayout.setupWithViewPager(viewPager);//绑定tab和viewPager
    }

}

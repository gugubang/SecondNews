package com.example.zzb.secondnews;

import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Toast;

import com.hyphenate.EMCallBack;
import com.hyphenate.chat.EMClient;

import cn.bmob.v3.Bmob;
import cn.bmob.v3.BmobUser;
import cn.bmob.v3.exception.BmobException;
import cn.bmob.v3.listener.SaveListener;
import database.MydatabaseHelper;
public class login extends AppCompatActivity implements View.OnClickListener {
    public static MydatabaseHelper datahelper;
    private Button load,zhuce,test;
    private EditText pas,name;
    private CheckBox check;
    public static SharedPreferences pre;
    private   SharedPreferences.Editor editor;
    private SQLiteDatabase db;
    private long exitTime = 0;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        pas=findViewById(R.id.loadpas);
        name=findViewById(R.id.loadname);
        load=findViewById(R.id.denglu);
        zhuce=findViewById( R.id.zhuce);
        load.setOnClickListener(this);
        check=findViewById(R.id.check);
        zhuce.setOnClickListener(this);
        test=findViewById(R.id.test);
        test.setOnClickListener(this);
        Bmob.initialize(login.this,"77c5c5f2c801b0fd46c20a07a6e1b90c");
        datahelper=new MydatabaseHelper(this,"User",null,1);
        db= datahelper.getWritableDatabase();
        pre=PreferenceManager.getDefaultSharedPreferences(this);
        boolean isremember=pre.getBoolean("rmemberpas",false);
        if(isremember){
            name.setText(pre.getString("username",""));
            pas.setText(pre.getString("password",""));
            check.setChecked(true);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.test:
                Intent intent_test=new Intent(login.this,MainActivity.class);
                Toast.makeText(login.this,"欢迎小甜糕",Toast.LENGTH_LONG).show();
                startActivity(intent_test);
                break;
            case R.id.denglu:
                String myname=name.getText().toString();
                String mypsw=pas.getText().toString();
                BmobUser p1=new BmobUser();
                p1.setUsername(myname);
                p1.setPassword(mypsw);
                p1.login(new SaveListener<BmobUser>() {

                    @Override
                    public void done(BmobUser bmobUser, BmobException e) {
                        if(e==null){
                            EMClient.getInstance().login(myname, mypsw, new EMCallBack() {
                                @Override
                                public void onSuccess() {
                                    EMClient.getInstance().groupManager().loadAllGroups();
                                    EMClient.getInstance().chatManager().loadAllConversations();
                                    editor=pre.edit();
                                    if(check.isChecked()){
                                        editor.putBoolean("rmemberpas",true);
                                        editor.putString("username",myname);
                                        editor.putString("password",mypsw);
                                    }else {
                                        editor.clear();
                                    }
                                    editor.commit();
                                    Log.d("zzzzzzzzzz", "登录聊天服务器成功！");

                                    Intent intent=new Intent(login.this,MainActivity.class);
                                    intent.putExtra("headusername",myname);
                                    startActivity(intent);
                                    runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            Toast.makeText(login.this,"登录服务器和数据库成功！！",Toast.LENGTH_SHORT).show();
                                        }
                                    });
                                    finish();
                                }

                                @Override
                                public void onError(int i, String s) {
                                    Log.d("zzzzzzzzzz", s);
                                    runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            Toast.makeText(login.this,"登录服务器失败！！",Toast.LENGTH_SHORT).show();
                                        }
                                    });
                                }

                                @Override
                                public void onProgress(int i, String s) {

                                }
                            });

                        }else {
                            Toast.makeText(login.this,"账号密码错误，登录数据库失败",Toast.LENGTH_SHORT).show();
                        }
                    }
                });
                break;
            case R.id.zhuce:
                Intent intent=new Intent(this,register.class);
                startActivity(intent);
                break;
        }
    }
}

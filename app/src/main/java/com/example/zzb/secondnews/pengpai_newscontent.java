package com.example.zzb.secondnews;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;

import reptile.javabean.pengpai_videocontentbean;

public class pengpai_newscontent extends AppCompatActivity {
    private WebView webView;
    private WebSettings webSettings;
    private FrameLayout frameLayout;
    private LinearLayout loading,empty,error;
    String javascript =  "javascript:function getClass(parent,sClass){" +
            "var aEle=parent.getElementsByTagName('div');" +
            "var aResult=[];" +
            "var i=0;" +
            "for(i<0;i<aEle.length;i++){" +
            "if(aEle[i].className==sClass){" +
            "aResult.push(aEle[i]);" +
            "}" +
            "};" +
            "return aResult;}" +
            "function hideOther() {" +
            "getClass(document,'head hdbk')[0].style.display='none';" +
            "getClass(document,'news_imgad')[0].style.display='none';" +
            "getClass(document,'news_tit2')[0].style.display='none';" +
            "getClass(document,'ctread_bd')[0].style.display='none';" +
            "getClass(document,'about')[0].style.display='none';" +
            "getClass(document,'news_path')[0].style.display='none';" +
            "getClass(document,'copyright')[0].style.display='none';" +
            "getClass(document,'main_rt')[0].style.display='none';}";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pengpai_newscontent);
        webView=findViewById(R.id.pengpai_web);
        frameLayout=findViewById(R.id.pengpai_newscontentFrame);
        loading=(LinearLayout)findViewById(R.id.loading);
        empty=(LinearLayout)findViewById(R.id.empty);
        error=(LinearLayout)findViewById(R.id.error);
        webView.setWebViewClient(new WebViewClient(){
            @Override
            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(view, url);
                //创建方法
                view.loadUrl(javascript);

                //加载方法
                view.loadUrl("javascript:hideOther();");}
        });
        webSettings=webView.getSettings();
        webSettings.setUserAgentString("Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/43.0.2357.134 Safari/537.36");
        webSettings.setJavaScriptEnabled(true);  //开启javascript
        String url=getIntent().getStringExtra("url");
        Log.d("zzzzzzz",url);
        //自适应屏幕
        webSettings.setUseWideViewPort(true);
        webSettings.setLoadWithOverviewMode(true);
        webSettings.setBuiltInZoomControls(true);
        webSettings.setSupportZoom(true); // 支持缩放
        webView.setInitialScale(125);
        webView.loadUrl(url);
        init();
    }
    public void init(){
        webView.setWebChromeClient(new WebChromeClient(){

            @Override
            public void onProgressChanged(WebView view, int newProgress) {
                if(newProgress<100)
                {
                    frameLayout.setVisibility(View.VISIBLE);
                    loading.setVisibility(View.VISIBLE);
                    empty.setVisibility(View.GONE);
                    error.setVisibility(View.GONE);
                }
                else {
                    frameLayout.setVisibility(View.GONE);
                    loading.setVisibility(View.GONE);
                    empty.setVisibility(View.GONE);
                    error.setVisibility(View.GONE);
                }
                super.onProgressChanged(view, newProgress);
            }
        });
    }
}

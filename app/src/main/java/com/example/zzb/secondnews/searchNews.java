package com.example.zzb.secondnews;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import Adapter.search_result_Adapter;
import com.scwang.smartrefresh.layout.SmartRefreshLayout;
import com.scwang.smartrefresh.layout.api.RefreshLayout;
import com.scwang.smartrefresh.layout.listener.OnLoadmoreListener;

import Fragment.FirstFragment;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import reptile.Manager.search_result_manager;
import reptile.javabean.NewsListBean;

import java.io.IOException;
import java.security.SecureRandom;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.List;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

public class searchNews extends AppCompatActivity {
    private EditText search_word;
    private ImageView search;
    private TextView search_result;
    private Button back;
    private ListView listView;
    private static String url;
    private search_result_Adapter adapter;
    private  List<NewsListBean> list=new ArrayList<NewsListBean>();
    private FrameLayout my_loading_frameyout;
    private LinearLayout loading,empty,error;
    private SmartRefreshLayout smartRefreshLayout;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_news);
        search_word=findViewById(R.id.edit_search);
        search=findViewById(R.id.news_search);
        search_result=findViewById(R.id.search_result);
        back=findViewById(R.id.search_back);
        listView=findViewById(R.id.search_list);
        smartRefreshLayout=findViewById(R.id.search_smart);
        loading=findViewById(R.id.loading);
        empty=findViewById(R.id.empty);
        error=findViewById(R.id.error);
        my_loading_frameyout=findViewById(R.id.search_loading);
        smartRefreshLayout=findViewById(R.id.search_smart);
        init();

    }
    public void  init(){
        my_loading_frameyout.setVisibility(View.VISIBLE);
        loading.setVisibility(View.GONE);
        empty.setVisibility(View.VISIBLE);
        error.setVisibility(View.GONE);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(search_word.getText().toString().equals("")){
                    Toast.makeText(searchNews.this,"请输入搜索关键字！",Toast.LENGTH_SHORT).show();
                    return;
                }
                String word=search_word.getText().toString().trim();
                url="https://search.cctv.com/search.php?qtext="+word+"&type=web";
                OkHttpClient.Builder mBuilder = new OkHttpClient.Builder();
                mBuilder.sslSocketFactory(createSSLSocketFactory());
                mBuilder.hostnameVerifier(new TrustAllHostnameVerifier());
                OkHttpClient okHttpClient = mBuilder.build();
                Request request=new Request.Builder().url(url).method("GET",null).build();
                Call call=okHttpClient.newCall(request);
                call.enqueue(new Callback() {
                    @Override
                    public void onResponse(Call call, Response response) throws IOException {
                        try {
                            list=new search_result_manager().getresult(url);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        Log.d("zzzzzz","列表获取成功");
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                try {
                                    Toast.makeText(searchNews.this,"网速较慢，请耐心等待！",Toast.LENGTH_SHORT).show();
                                    Log.d("zzzzzz",url);
                                    adapter=new search_result_Adapter(searchNews.this,R.layout.search_result_item,list);
                                    Log.d("zzzzzz","适配器成功");
                                    listView.setAdapter(adapter);
                                    Toast.makeText(searchNews.this,"搜索结果成功！",Toast.LENGTH_SHORT).show();
                                    Log.d("zzzzzz","success");
                                    if(list==null){
                                        Toast.makeText(searchNews.this,"搜索结果为空！",Toast.LENGTH_SHORT).show();
                                        return; }else {
                                        my_loading_frameyout.setVisibility(View.GONE);
                                        loading.setVisibility(View.GONE);
                                        empty.setVisibility(View.GONE);
                                        error.setVisibility(View.GONE);
                                    }
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }
                        });
                        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                            @Override
                            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                                NewsListBean bean=list.get(position);
                                Intent intent=new Intent(searchNews.this,pengpai_newscontent.class);
                                intent.putExtra("url", bean.getHref());
                                startActivity(intent);
                            }
                        });
                    }

                    @Override
                    public void onFailure(Call call, IOException e) {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                my_loading_frameyout.setVisibility(View.VISIBLE);
                                loading.setVisibility(View.GONE);
                                empty.setVisibility(View.GONE);
                                error.setVisibility(View.VISIBLE);
                                Log.d("zzzzzz",e.toString());
                                Log.d("zzzzzz","fail");
                            }
                        });
                        return;
                    }
                });
            }
        });

        smartRefreshLayout.setOnLoadmoreListener(new OnLoadmoreListener() {
            @Override
            public void onLoadmore(RefreshLayout refreshlayout) {
                Log.d("zzzzzzzzz","现在的URL："+url);
                OkHttpClient.Builder mBuilder = new OkHttpClient.Builder();
                mBuilder.sslSocketFactory(createSSLSocketFactory());
                mBuilder.hostnameVerifier(new TrustAllHostnameVerifier());
                OkHttpClient okHttpClient = mBuilder.build();
                Request request=new Request.Builder().url(url+"&vtime=&datepid=1&channel=&page=2").method("GET",null).build();

                Call call=okHttpClient.newCall(request);
                call.enqueue(new Callback() {
                    @Override
                    public void onResponse(Call call, Response response) throws IOException {
                        try {
                            List<NewsListBean> more=new search_result_manager().getresult(url+"&vtime=&datepid=1&channel=&page=2");
                            Log.d("ssssss",more.get(0).getTitle());
                            list.addAll(more);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                adapter.notifyDataSetChanged();
                            }
                        });
                    }
                    @Override
                    public void onFailure(Call call, IOException e) {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                my_loading_frameyout.setVisibility(View.VISIBLE);
                                loading.setVisibility(View.GONE);
                                empty.setVisibility(View.GONE);
                                error.setVisibility(View.VISIBLE);
                                Log.d("zzzzzz",e.toString());
                                Log.d("zzzzzz","fail");
                            }
                        });
                        return;
                    }
                });
            }
        });
    }
    private static SSLSocketFactory createSSLSocketFactory() {
        SSLSocketFactory ssfFactory = null;

        try {
            SSLContext sc = SSLContext.getInstance("TLS");
            sc.init(null,  new TrustManager[] { new TrustAllCerts() }, new SecureRandom());

            ssfFactory = sc.getSocketFactory();
        } catch (Exception e) {
        }

        return ssfFactory;
    }


    private static class TrustAllCerts implements X509TrustManager {
        @Override
        public void checkClientTrusted(X509Certificate[] chain, String authType) throws CertificateException {}

        @Override
        public void checkServerTrusted(X509Certificate[] chain, String authType) throws CertificateException {}

        @Override
        public X509Certificate[] getAcceptedIssuers() {return new X509Certificate[0];}
    }

    private static class TrustAllHostnameVerifier implements HostnameVerifier {
        @Override
        public boolean verify(String hostname, SSLSession session) {
            return true;
        }
    }
}

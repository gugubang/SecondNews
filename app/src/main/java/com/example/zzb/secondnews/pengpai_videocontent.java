package com.example.zzb.secondnews;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.io.IOException;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import reptile.Manager.pengpai_videoManager;
import reptile.javabean.pengpai_videocontentbean;

public class pengpai_videocontent extends AppCompatActivity {
    private WebView webView;
    private TextView videotitle,videocontent,videotime,videosource;
    private String url;
    private FrameLayout frameLayout;
    private LinearLayout loading,empty,error;
    private pengpai_videocontentbean bean;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pengpai_videocontent);
        webView=findViewById(R.id.pengpai_video_web);
        videotitle=findViewById(R.id.pengpai_video_title);
        videocontent=findViewById(R.id.pengpai_video_content);
        videosource=findViewById(R.id.pengpai_video_source);
        videotime=findViewById(R.id.pengpai_video_time);
        webView.setWebChromeClient(new WebChromeClient());
        webView.setWebViewClient(new WebViewClient());
        frameLayout=findViewById(R.id.pengpai_videocontentFrame);
        loading=(LinearLayout)findViewById(R.id.loading);
        empty=(LinearLayout)findViewById(R.id.empty);
        error=(LinearLayout)findViewById(R.id.error);
        init();
    }
    public void init(){
        frameLayout.setVisibility(View.VISIBLE);
        loading.setVisibility(View.VISIBLE);
        empty.setVisibility(View.GONE);
        error.setVisibility(View.GONE);
        OkHttpClient okHttpClient=new OkHttpClient();
        url=getIntent().getStringExtra("url");
        final Request request = new Request.Builder().url(url).method("GET",null).build();
        Call call=okHttpClient.newCall(request);
        call.enqueue(new Callback() {
            @Override
            public void onResponse(Call call, Response response) throws IOException {
                bean=new pengpai_videoManager().getvideo(url);
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        webView.loadUrl(bean.getPlayUrl());
                        videotitle.setText(bean.getTitle());
                        videocontent.setText(bean.getContent());
                        videotime.setText(bean.getTime());
                        videosource.setText(bean.getSource());
                        if(bean==null){
                            frameLayout.setVisibility(View.VISIBLE);
                            loading.setVisibility(View.GONE);
                            empty.setVisibility(View.VISIBLE);
                            error.setVisibility(View.GONE);
                        }else {
                            frameLayout.setVisibility(View.GONE);
                            loading.setVisibility(View.GONE);
                            empty.setVisibility(View.GONE);
                            error.setVisibility(View.GONE);
                        }
                    }
                });

            }

            @Override
            public void onFailure(Call call, IOException e) {
                frameLayout.setVisibility(View.VISIBLE);
                loading.setVisibility(View.GONE);
                empty.setVisibility(View.GONE);
                error.setVisibility(View.VISIBLE);
                return;
            }
        });
    }
}

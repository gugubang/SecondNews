package Adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.zzb.secondnews.R;
import com.example.zzb.secondnews.message;
import java.util.List;
//发送和接收消息的消息实体类
public class MsgAdapter extends RecyclerView.Adapter<MsgAdapter.ViewHolder> {
    private List<message> mylist;
    public MsgAdapter(List<message> mylist){
        this.mylist=mylist;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {

       View view=LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.chat_item,viewGroup,false);
       return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {
    message msg=mylist.get(i);
    if(msg.getType()==message.TYPE_RECEIVED){
        viewHolder.left.setVisibility(View.VISIBLE);
        viewHolder.right.setVisibility(View.GONE);
        viewHolder.leftmsg.setText(msg.getContent());
    }else  if(msg.getType()==message.TYPE_SEND){
        viewHolder.left.setVisibility(View.GONE);
        viewHolder.right.setVisibility(View.VISIBLE);
        viewHolder.rightmsg.setText(msg.getContent());
    }
    }

    @Override
    public int getItemCount() {
        return mylist.size();
    }

    class ViewHolder extends  RecyclerView.ViewHolder{
        RelativeLayout left,right;
        TextView leftmsg,rightmsg;
        public ViewHolder(View view){
            super(view);
            left=view.findViewById(R.id.left_layout);
            right=view.findViewById(R.id.right_layout);
            leftmsg=view.findViewById(R.id.leftmessage);
            rightmsg=view.findViewById(R.id.rightmessage);

        }
    }
}

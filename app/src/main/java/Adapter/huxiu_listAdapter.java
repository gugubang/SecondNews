package Adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.example.zzb.secondnews.R;

import java.util.List;

import reptile.javabean.NewsListBean;

public class huxiu_listAdapter extends ArrayAdapter<NewsListBean> {
    public huxiu_listAdapter(Context context, int resource, List<NewsListBean> newsListBeans) {
        super(context, resource,newsListBeans);
    }
    class ViewHolder{
        ImageView imageView;
        TextView titleView;
        TextView SourceView;
        TextView dataVIew;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final NewsListBean newsListBean=getItem(position);
        View view;
        RequestOptions options = new RequestOptions()//图片加载设置加载失败，加载中的过度图片
                .placeholder(R.drawable.loadingimage).error(R.drawable.loadimageerror).fitCenter();
        ViewHolder viewholder;
        if(convertView==null) {
            view = LayoutInflater.from(getContext()).inflate(R.layout.huxiu_item, parent, false);
            viewholder = new ViewHolder();
            viewholder.imageView = view.findViewById(R.id.huxiu_img);
            viewholder.titleView = view.findViewById(R.id.huxiu_title);
            viewholder.SourceView = view.findViewById(R.id.huxiu_source);
            viewholder.dataVIew = view.findViewById(R.id.huxiu_time);
            view.setTag(viewholder);
        }else {
            view=convertView;
            viewholder=(ViewHolder) view.getTag();
        }
        Log.d("adapter","success");
        viewholder.titleView.setText(newsListBean.getTitle());
        viewholder.SourceView.setText(newsListBean.getSource());
        Glide.with(getContext()).load(newsListBean.getImgurl()).apply(options).into(viewholder.imageView);
        viewholder.dataVIew.setText(newsListBean.getDatetime());
        return view;
    }
}

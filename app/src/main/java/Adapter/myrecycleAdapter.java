package Adapter;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.example.zzb.secondnews.R;

import reptile.javabean.NewsListBean;
import java.util.List;
//首页的其他分类的新闻recycleview的适配器
public class myrecycleAdapter extends RecyclerView.Adapter<myrecycleAdapter.viewHolder> implements View.OnClickListener, View.OnLongClickListener {
    private List<NewsListBean> NewsListBeans;
    private Context mContext;
    public static int click;
    public myrecycleAdapter(Context mContext,List<NewsListBean> NewsListBeans){
        this.mContext=mContext;
        this.NewsListBeans=NewsListBeans;
    }


    @Override
    public void onClick(View v) {
        if(onitemClickListener!=null){
        onitemClickListener.OnItemClick(v,(NewsListBean) v.getTag());
        }
    }

    @Override
    public boolean onLongClick(View v) {

        if(longClickLisenter != null)
        {
            longClickLisenter.LongClickLisenter(v,(NewsListBean) v.getTag());
        }
        return false;
    }

    static class viewHolder extends RecyclerView.ViewHolder{
        View itemView;
        ImageView imageView;
        TextView titleView;
        // TextView contentView;
        TextView SourceView;
        TextView guanjianziView;
        TextView dataVIew;
        public viewHolder(View myview){
            super(myview);
            itemView=myview;
          imageView = myview.findViewById(R.id.item_image);
            titleView = myview.findViewById(R.id.item_title);
            //contentView=view.findViewById(R.id.item_content);
            SourceView = myview.findViewById(R.id.source);
          guanjianziView = myview.findViewById(R.id.guanjianzi);
          dataVIew = myview.findViewById(R.id.time);
        }
    }
    @Override
    public viewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view=LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.list_item,viewGroup,false);
        final viewHolder viewHolder=new viewHolder(view);
        view.setOnClickListener(this);
        view.setOnLongClickListener(this);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder( viewHolder viewHolder, int i) {

        RequestOptions options = new RequestOptions()//图片加载设置加载失败，加载中的过度图片
                .placeholder(R.drawable.loadingimage).error(R.drawable.loadimageerror).fitCenter();
        NewsListBean news=NewsListBeans.get(i);
        viewHolder.itemView.setTag(news);
           // Log.d("zzb",news.getTitle());
        viewHolder.titleView.setText(news.getTitle());
        viewHolder.SourceView.setText(news.getSource());
        Glide.with(mContext).load(news.getImgurl()).apply(options).into(viewHolder.imageView);
        StringBuffer tag=news.getTag();
        viewHolder.guanjianziView.setText(tag);
        viewHolder.dataVIew.setText(news.getDatetime());
    }

    @Override
    public int getItemCount() {
        return NewsListBeans.size();
    }

    public void  addData(int postion){
        NewsListBeans.add(new NewsListBean());
        notifyItemInserted(postion);
        notifyDataSetChanged();
    }
    public void  removeData(int postion){
        NewsListBeans.remove(postion);
        notifyItemRemoved(postion);
        notifyDataSetChanged();
    }

    public interface OnitemClickListener{
        void OnItemClick(View view,NewsListBean newsListBean);
    }
    public interface LongClickLisenter {
        void LongClickLisenter(View view,NewsListBean newsListBean);
    }
    private OnitemClickListener onitemClickListener;
    private LongClickLisenter longClickLisenter;

    public void setLongClickLisenter(LongClickLisenter longClickLisenter) {
        this.longClickLisenter = longClickLisenter;
    }
    public void setOnitemClickListener(OnitemClickListener onitemClickListener) {
        this.onitemClickListener = onitemClickListener;
    }
}


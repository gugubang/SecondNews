package Adapter;
import reptile.javabean.videoBean;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.example.zzb.secondnews.R;

import java.util.List;
//首页视频列表的适配器
public class video_recycle_adapter extends RecyclerView.Adapter<video_recycle_adapter.ViewHolder> implements View.OnClickListener, View.OnLongClickListener {
private List<videoBean> list;
private Context mContex;
public video_recycle_adapter(Context mContex,List<videoBean> list){
   this.list=list;
   this.mContex=mContex;
}


    @Override
    public void onClick(View v) {
        if(onitemClickListener!=null){
            onitemClickListener.OnItemClick(v,(videoBean) v.getTag());
        }
    }

    @Override
    public boolean onLongClick(View v) {
    if(longClickLisenter != null){
        longClickLisenter.LongClickLisenter(v,(videoBean) v.getTag());
    }
        return false;
    }

    public interface OnitemClickListener{
    void OnItemClick(View view,videoBean videoBean);
}
    public interface LongClickLisenter {
        void LongClickLisenter(View view,videoBean videoBean);
    }
private OnitemClickListener onitemClickListener;
private LongClickLisenter longClickLisenter;

    public void setLongClickLisenter(LongClickLisenter longClickLisenter) {
        this.longClickLisenter = longClickLisenter;
    }

    public void setOnitemClickListener(OnitemClickListener onitemClickListener) {
        this.onitemClickListener = onitemClickListener;
    }

    final RequestOptions options = new RequestOptions()//图片加载设置加载失败，加载中的过度图片
           .placeholder(R.drawable.loadingimage).error(R.drawable.loadimageerror).fitCenter();




@Override
   public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
   View view=LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.video_list_item,viewGroup,false);
   ViewHolder holder=new ViewHolder(view);
   view.setOnClickListener(this);
   view.setOnLongClickListener(this);
      return holder;
   }

   @Override
   public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {
      videoBean bean=list.get(i);
      viewHolder.itemview.setTag(bean);
      //Glide.with(mContex).load(bean.getBackgroundurl()).apply(options).into(viewHolder.videoImg);
      viewHolder.videoImg.loadUrl(bean.getVideourl());
       viewHolder.videotitle.setText(bean.getTitle());
      viewHolder.videodes.setText(bean.getDescription());
      viewHolder.videocount.setText("视频评分："+bean.getCount());
//      viewHolder.videotime.setText(bean.getTime());
   }

   @Override
   public int getItemCount() {
      return list.size();
   }

   class ViewHolder extends RecyclerView.ViewHolder{
    View itemview;
      WebView videoImg;
      TextView videotitle;
      TextView videodes;
      TextView videocount;
      TextView videotime;
   public ViewHolder(View view){
      super(view);
      itemview=view;
      videoImg=view.findViewById(R.id.video_img);
       videoImg.getSettings().setCacheMode(WebSettings.LOAD_CACHE_ELSE_NETWORK);
       videoImg.setWebChromeClient(new WebChromeClient());
      videotitle=view.findViewById(R.id.video_title);
      videodes=view.findViewById(R.id.video_description);
      videocount=view.findViewById(R.id.video_count);
     // videotime=view.findViewById(R.id.video_time);
   }
   }
}

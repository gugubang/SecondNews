package Adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import java.util.List;

import reptile.javabean.tabBean;

//澎湃新闻viewPager绑定fragment的适配器
public class pengpaifragmentAdapter extends FragmentStatePagerAdapter {
    private List<Fragment> mfragment;
    private List<tabBean> mtabs;

    // private myFragment myFragment=null;
       public pengpaifragmentAdapter(FragmentManager fm, List<Fragment> fragments, List<tabBean> tabs) {
           super(fm);
           mfragment = fragments;
           mtabs=tabs;
       }


    @Override
    public Fragment getItem(int i) {
        return mfragment.get(i);
    }
    @Override
    public CharSequence getPageTitle(int position) {
        return mtabs.get(position).getTabName();
    }
    @Override
    public int getCount() {
        return mfragment.size();
    }

}

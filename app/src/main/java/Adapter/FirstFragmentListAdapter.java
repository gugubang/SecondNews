package Adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.example.zzb.secondnews.R;

import java.util.List;

import Fragment.FirstFragment;
import reptile.javabean.NewsListBean;
import reptile.javabean.tagbean;
//新闻首页的listview的适配器
public class FirstFragmentListAdapter extends ArrayAdapter<NewsListBean> {

    private int ID;
    public FirstFragmentListAdapter(Context context, int resource, List<NewsListBean> newsListBeans) {
        super(context, resource,newsListBeans);
        ID=resource;

    }
  class ViewHolder{
      ImageView imageView;
      TextView titleView;
      // TextView contentView;
     TextView SourceView;
       TextView guanjianziView;
      TextView dataVIew;
  }

    @Override
    public View getView(int position, View convertView,  ViewGroup parent) {
       final NewsListBean newsListBean=getItem(position);
       View view;
        RequestOptions options = new RequestOptions()//图片加载设置加载失败，加载中的过度图片
                .placeholder(R.drawable.loadingimage).error(R.drawable.loadimageerror).fitCenter();
       ViewHolder viewholder;
        if(convertView==null) {
            view = LayoutInflater.from(getContext()).inflate(R.layout.list_item, parent, false);
            viewholder = new ViewHolder();
            viewholder.imageView = view.findViewById(R.id.item_image);
            viewholder.titleView = view.findViewById(R.id.item_title);
            //contentView=view.findViewById(R.id.item_content);
            viewholder.SourceView = view.findViewById(R.id.source);
            viewholder.guanjianziView = view.findViewById(R.id.guanjianzi);
            viewholder.dataVIew = view.findViewById(R.id.time);
            view.setTag(viewholder);
        }else {
            view=convertView;
            viewholder=(ViewHolder) view.getTag();
        }
        viewholder.titleView.setText(newsListBean.getTitle());
        viewholder.SourceView.setText(newsListBean.getSource());
        Glide.with(getContext()).load(newsListBean.getImgurl()).apply(options).into(viewholder.imageView);
            StringBuffer tag=newsListBean.getTag();
            viewholder.guanjianziView.setText(tag);

       viewholder.dataVIew.setText(newsListBean.getDatetime());
        return view;
    }


}

package Adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.example.zzb.secondnews.R;

import java.util.List;
import reptile.javabean.pengpaiNewsListBean;
public class pengpairecycleadapter extends RecyclerView.Adapter<pengpairecycleadapter.viewHolder> implements View.OnClickListener, View.OnLongClickListener {
    private List<pengpaiNewsListBean> NewsListBeans;
    private Context mContext;
    private View mHeaderView;
    public pengpairecycleadapter(Context mContext, List<pengpaiNewsListBean> NewsListBeans){
        this.mContext=mContext;
        this.NewsListBeans=NewsListBeans;
    }



    static class viewHolder extends RecyclerView.ViewHolder {
        View itemView;
        ImageView imageView;
        TextView titleView;
        TextView contentView;
        TextView SourceView;
        TextView dataVIew;
        public viewHolder(View myview){
            super(myview);
            itemView=myview;
            imageView = myview.findViewById(R.id.pengpai_img);
            titleView = myview.findViewById(R.id.pengpai_title);
            contentView=myview.findViewById(R.id.pengpai_content);
            SourceView = myview.findViewById(R.id.pengpai_soure);
            dataVIew = myview.findViewById(R.id.pengpai_time);
        }
    }
    @Override
    public viewHolder onCreateViewHolder( ViewGroup viewGroup, int i) {
        View view=LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.pengpai_recycle_item,viewGroup,false);
        final viewHolder viewHolder=new viewHolder(view);
        view.setOnClickListener(this);
        view.setOnLongClickListener(this);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder( viewHolder viewHolder, int i) {
        RequestOptions options = new RequestOptions()//图片加载设置加载失败，加载中的过度图片
                .placeholder(R.drawable.loadingimage).error(R.drawable.loadimageerror).fitCenter();
        pengpaiNewsListBean news=NewsListBeans.get(i);
        viewHolder.itemView.setTag(news);
        // Log.d("zzb",news.getTitle());
        viewHolder.titleView.setText(news.getTitle());
        viewHolder.SourceView.setText(news.getSource());
        Glide.with(mContext).load(news.getImgurl()).apply(options).into(viewHolder.imageView);
        viewHolder.contentView.setText(news.getContent());
        viewHolder.dataVIew.setText(news.getDatetime());
    }

    @Override
    public int getItemCount() {
        return NewsListBeans.size();
    }

    /**
     * Called when a view has been clicked.
     *
     * @param v The view that was clicked.
     */
    @Override
    public void onClick(View v) {
        if(onitemClickListener!=null){
            onitemClickListener.OnItemClick(v,(pengpaiNewsListBean) v.getTag());
        }
    }


    @Override
    public boolean onLongClick(View v) {
        if(longClickLisenter != null)
        {
            longClickLisenter.LongClickLisenter(v,(pengpaiNewsListBean) v.getTag());
        }
        return false;
    }



    public interface OnitemClickListener{
        void OnItemClick(View view, pengpaiNewsListBean newsListBean);
    }
    public interface LongClickLisenter {
        void LongClickLisenter(View view, pengpaiNewsListBean newsListBean);
    }
    private OnitemClickListener onitemClickListener;
    private LongClickLisenter longClickLisenter;

    public void setLongClickLisenter(LongClickLisenter longClickLisenter) {
        this.longClickLisenter = longClickLisenter;
    }
    public void setOnitemClickListener(OnitemClickListener onitemClickListener) {
        this.onitemClickListener = onitemClickListener;
    }
}

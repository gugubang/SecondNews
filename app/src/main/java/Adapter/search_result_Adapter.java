package Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.example.zzb.secondnews.R;

import reptile.javabean.NewsListBean;
import java.util.List;
//搜索结果的列表适配器
public class search_result_Adapter extends ArrayAdapter<NewsListBean> {
    public search_result_Adapter(Context context, int resource, List<NewsListBean> newsListBeans){
        super(context, resource,newsListBeans);
    }
    class ViewHolder{
        ImageView imageView;
        TextView titleView;
        TextView contentView;
        TextView SourceView;
        TextView dataVIew;
    }

    @Override
    public View getView(int position,View convertView,ViewGroup parent) {
        final NewsListBean newsListBean=getItem(position);
        View view;
        RequestOptions options = new RequestOptions()//图片加载设置加载失败，加载中的过度图片
                .placeholder(R.drawable.loadingimage).error(R.drawable.loadimageerror).fitCenter();
        ViewHolder viewholder;
        if(convertView==null) {
            view = LayoutInflater.from(getContext()).inflate(R.layout.search_result_item, parent, false);
            viewholder = new ViewHolder();
            viewholder.imageView = view.findViewById(R.id.search_img);
            viewholder.titleView = view.findViewById(R.id.search_title);
            viewholder.contentView=view.findViewById(R.id.search_content);
            viewholder.SourceView = view.findViewById(R.id.search_source);
            viewholder.dataVIew = view.findViewById(R.id.search_time);
            view.setTag(viewholder);
        }else {
            view=convertView;
            viewholder=(ViewHolder) view.getTag();
        }
        viewholder.titleView.setText(newsListBean.getTitle());
        viewholder.SourceView.setText(newsListBean.getSource());
        Glide.with(getContext()).load(newsListBean.getImgurl()).apply(options).into(viewholder.imageView);
        viewholder.dataVIew.setText(newsListBean.getDatetime());
        viewholder.contentView.setText(newsListBean.getContent());
        return view;
    }
}

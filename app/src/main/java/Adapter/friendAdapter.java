package Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.example.zzb.secondnews.R;

import java.util.List;
//聊天好友列表的适配器
public class friendAdapter extends ArrayAdapter<String> {
    private List<String> friends;
    public friendAdapter(Context context, int resource, List<String> friends) {
        super(context, resource,friends);
        this.friends=friends;
    }

    @Override
    public View getView(int position, View convertView,  ViewGroup parent) {
        String name=getItem(position);
        View view=LayoutInflater.from(getContext()).inflate(R.layout.list_friend_item,parent,false);
        TextView nametext=view.findViewById(R.id.friend_name);
        nametext.setText(name);
        return view;
    }
}

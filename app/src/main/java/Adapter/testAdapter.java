package Adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.zzb.secondnews.R;

import java.util.List;

public class testAdapter extends RecyclerView.Adapter<testAdapter.ViewHoder> {
private List<String> str;

    @NonNull
    @Override
    public ViewHoder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view=LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.test_list_item,viewGroup,false);
        ViewHoder viewHoder=new ViewHoder(view);
        return viewHoder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHoder viewHoder, int i) {
    String s=str.get(i);
    viewHoder.imageView.setImageResource(R.drawable.hotlist);
    viewHoder.textView.setText(s);
    }

    @Override
    public int getItemCount() {
        return str.size();
    }


    static class ViewHoder extends RecyclerView.ViewHolder{
       ImageView imageView;
       TextView textView;
       public ViewHoder(View view){
           super(view);
           imageView=view.findViewById(R.id.fruit_image);
           textView=view.findViewById(R.id.fruit_name);
       }
   }
   public testAdapter(List<String> list){
       str=list;
   }
}
